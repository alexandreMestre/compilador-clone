## HOW TO USE DOCKER

To use docker we need to have acess to an image having all of the library that 
we are goint to use in the project. The Image is available in the DockerHub and 
you only need to pull the image to your PC and then start the container. 

# Installing Docker in your PC 
Open your terminal and type: `sudo apt-get install docker.io`. For MAC users, 
type: `sudo brew install docker.io`. 
Another option is to download directly from the docker site.

# Pulling the Image
Open your terminal and type `docker pull franciscorafael/docker-grupo06`.
To see all images you have in your PC, type `docker images`. Below REPOSITORY 
you might see the name `franciscorafael/docker-grupo06` and below TAG you might
see the name `latest`. This indicates that your image has been sucesfully 
downloaded and installed in your PC

# Starting the container
Once we have the image, we can start the container. To start it, type in the
terminal the following command: 

`docker run -ti --name [CONTAINER_NAME] --userns=host --rm 
--volume=PATH/TO/DIR:PATH/TO/DIR --volume=PATH/TO/DIR:PATH/TO/DIR -
w=$(pwd) franciscorafael/docker-grupo06:latest`

`-name` is a flag to indetify your container in the running containers. 
`--volume=PATH/TO/DIR` specifies the directories that your container will have
acess. For example, im my case, I would like to create a container that will 
see my MC911 directory and my Donwloads directory. Then, I type:

`docker run -ti --name gabriel_MC911 --userns=host --rm 
--volume=/home/gabriel/MC911/:/home/gabriel/MC911/ 
--volume=/home/gabriel/Downloads/:/home/gabriel/Downloads/ -
w=$(pwd) franciscorafael/docker-grupo06:latest`

Afeter that, you will se a bash line indicating that the container is running
and ready to be used. 	

# Removing the container

When we start a container, it starts to use the memory of your PC in order to 
execute all of your codes and scripts. So, if you get out of the container 
without killing it, your container will keep running and occupying your memory,
that is why we added the flag `-rm` in the docker creation, indicating that the
container will be killed each time you get out of it. Note that all of your
files that you have created and updated will not be lost if you saved or commited 
them to the GitLab respository. To get out of container, type: `Ctrl+D` in the
bash inside the container. To see if your container is still running or not, 
type: `docker ps`, then all of the running containers will be listed. You have
only to search for your container name (specified in the creation in the flag
`-name`) in the last column. If you dont see your continer there, it means
taht your container was sucessfully killed. To create the container again, 
you have only to follow the step of the previous section again. 

## Como usar

<p>Um docker foi criado no Docker hub, um sistema que pode armazenar as imagens e também ter um controle de versões, podendo haver edições por parte de todos os usuários.</p>

O link para o docker hub é: 
>  [Imagem - Docker Hub](https://hub.docker.com/r/franciscorafael/docker-grupo06)

<p>Você pode checar como utilizar o docker sem o Makefile e também como instala-lo em seu computador em: [README.md ](https://gitlab.ic.unicamp.br/mc911/2019-s1/grupo-06/blob/docker/README.md)</p>

<p> Os principais comandos para executar que estão inclus no Makefile são: </p>


```
sudo make pull (se você tiver em um ambiente linux)
make pull (nos demais)
```


<p>Esse comando fará com que você faça o download da imagem do docker em seu computador. </p>


```
sudo make run (se você tiver em um ambiente linux)
make run (nos demais)
```


<p> Esse comando executará o docker e após exectutá-lo, você encontrará dentro do ambiente Linux (baseado no Ubuntu 18.04). Aqui, o docker irá inicializar no Diretório `HOME` e a partir dele voce pode navegar
até o diretório do seu projeto e utilizar suas funcionalidades.</p>

### Funcionalidades presente no Docker. 

Essa é baseada no Ubuntu 18.04 e contém suas funcionalidades padrão. Além disso, inclui:
*  Git: Pode-se utilizar dos comandos git. 
*  Java SDK 8: Todas as funcionalidades do Java 8
*  GCC e GDB: Para compilação de códigos C. 

<p> Além dessas fucionalidades, tem: </p>

> **JAVACC: Java Compiler Compilar**

Pode-se usar o comando `javacc` para executá-lo e assim todas suas funções.

> **Python: Sistema de execução Python**

Comandos: 
* python para exetuctar python 2.7 
* python3.6 para executar python 3.6

> **Kotlin: Sistema de execução Kotlin**

<p>Como exectuar um programa em kotlin no docker usando kotlinc: </p>

Dado o código em kotlin abaixo no arquivo Main.kt:

```
fun main(args: Array<String>) {
    println("Hello, World!")
}
```

Pode-se executar: 

`$ kotlinc hello.kt -include-runtime -d hello.jar`

E após de gerar o arquivo '.jar':

`$ java -jar hello.jar`

E assim ver a saída esperada.



> **Gradle 2.6** 

Sistema gradle para poder utilizar e rodar códigos da linguagem Kotlin. Segue alguns comandos: 
*  grandle build (para gerar uma build)
*  gradble run (para executar a build gerada)


