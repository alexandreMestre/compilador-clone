# Planejamento Do Projeto: GRUPO 06

| Nome               | RA     |
| ------------------ | ------ |
| Alexandre Mestre   | 154502 |
| Danilo Charântola  | 155124 |
| Felipe Eulálio     | 155299 |
| Francisco Carneiro | 157888 |
| Gabriel Bertocco   | 155419 |
| Rafael Zane        | 157079 |
| Rodrigo Máximo     | 157209 |

## 1. Descrição do Projeto

A ideia principal do projeto é gerar um arquivo c `(extensão .c)` a partir de um código fonte em Python `(extensão .py)`, tal que o arquivo de saída possua a mesma funcionalidade do arquivo de entrada, ou seja, ao se executar o arquivo em Python, sua resposta deverá ser a mesma da resposta após a execução do arquivo gerado em C. 
    
Obviamente que em um projeto de uma disciplina de graduação abordar todos os possíveis cenários para ambas as linguagens seria algo um pouco complexo e portanto, a ideia principal é abordar um pequeno escopo da linguagem Python, utilizando apenas algumas heurísticas que possam ser convertidas para a linguagem C. Além disso, a medida em que o projeto se desenvolver, novas funcionalidades ou rotinas poderão ser adicionadas e desenvolvidas.

## 2. Desenvolvimento

###  Linguagem
A linguagem para a implementação deste projeto será Kotlin, uma linguagem de propósito geral e com inferência de tipo, que possui interoperabilidade total com código Java, isto é, arquivos e bibliotecas escritos em Java podem ser importados e reconhecidos por arquivos e bibliotecas escritos em Kotlin, e vice-versa.

### Versionamento e Integração Continua
O Gitlab será utilizado para manter a integridade do código e o controle de versionamento. A ideia principal é utilizar branches secundárias para que os códigos possam ser desenvolvidos e uma branch principal (development branch), onde as funcionalidades estáveis serão submetidas desde que possuam a aprovação  de membros da equipe e também não estejam falhando nenhum dos testes unitários.

Além disso, as Issues serão utilizadas para controle de requisitos desenvolvidos por cada membro e também para reportar eventuais bugs ou problemas encontrados.

A integração contínua juntamente com os testes unitários serão utilizados para manter a consistência do código principal (na development branch), prevenindo que códigos que contêm eventuais problemas possam ser submetidos para a development branch e também como forma de encontrar erros/bugs de forma rápida e eficiente. 

### Entrega e Ambiente de Execução

A ideia principal, é que o programa final possa ser executado em um docker. 

Docker é uma tecnologia que utiliza de contêineres para gerar uma virtualização, na qual o kernel da máquina onde o programa esta sendo executado é compartilhado com o kernel da máquina virtualizada e assim pode-se compartilhar bibliotecas e uma série de dependências que não está presente na máquina hospedeira. Assim, há uma maior flexibilidade para a execução do projeto em ambientes diferentes sem que ocorram eventuais problemas.

Dessa forma, a entrega final deve conter o código final junto com o Makefile de forma que a aplicação seja executada como esperado.  


### Arquitetura do sistema 

<imagem> 

Componentes do sistema:

* Analisador léxico: será utilizado para identificar padrões e gerar tokens referentes ao arquivo python de entrada. Será gerado a partir de um gerador de analisador léxico que receberá quais expressões regulares devem ser procurados no arquivo de entrada e quais ações tomar quando um padrão for reconhecido. Alguns exemplos de regras criadas serão: encontrar palavras que serão variáveis no programa, encontrar operadores como `+`, `-`, `*`, `/`, encontrar palavras reservadas da linguagem python como `for`, `while`, `def`. As ações a serem tomadas quando um padrão de uma regra for encontrado, dizem respeito a criação de tokens que correspondem as palavras reconhecidas com atribuição de significado.
* Analizador sintático: recebendo os tokens da linguagem python encontrados pelo analisador léxico, o analisador sintático tem como objetivo, através da definição de uma gramática formal, identificar quando uma sequência de tokens pode ser reduzida a uma das regras da gramática e o que fazer nessa situação. No nosso caso, a gramática possuirá um conjunto reduzido de regras referentes a linguagem python, de modo que atenda as funcionalidades definidas no escopo do projeto. Uma vez encontrada uma sequência que é reduzida em uma regra, será chamada uma ou mais ações na interface de conversão para C, responsável por converter aquela regra para a linguagem C.
* Interface de Conversão para C: esse componente terá funções que geram código na linguagem C. Serão definidas portanto, funções que criam variáveis de um determinado tipo, funções que definem um loop, funções que executam um operador entre duas expressões, dentre outras. A idea dessa interface basicamente é criar múltiplas funções com objetivos simples, de modo que cada uma possa ser utilizada por funções  mais complexas e que traduzem as diferentes regras encontradas.


## 3. Requisitos funcionais

Será desenvolvida uma simples aplicação com uma interface visual que permitirá ao usuário escolher entre os arquivos do computador no qual a aplicação está sendo executada, ou seja, um determinado arquivo Python a ser convertido para C. Além disso, será permitido ao usuário escolher onde salvar o arquivo C gerado antes que a conversão ocorra. E, caso o arquivo de entrada contenha erros ou variáveis fora do escopo do projeto (definido abaixo), a aplicação irá indicar visualmente um log no qual eles serão descritos.

## 4. Requisitos não funcionais

### Escopo

Como já dito anteriormente, esse projeto será capaz de identificar e converter para a linguagem C um subconjunto das definições e estruturas de dados presentes na linguagem Python. Abaixo é descrito quais são os componentes da linguagem Python que poderão ser reconhecidos pelo sistema para que sejam devidamente convertidos para o código C. Elementos fora do especificado abaixo serão reconhecidos como erro pelo sistema.

**Variáveis**

O código Python não apresenta declaração de tipos em suas variáveis, enquanto que em C os tipos estão presentes. Assim, será desenvolvido um identificador de qual tipo é uma variável que está sendo usada no programa, baseado na análise dos valores que esta variável recebe e como ela é utilizada ao longo do programa. Desta maneira pode-se criar e usar uma variável equivalente no código em C baseado no tipo indentificado. 

Em seguida, por meio da estensão do identificador de tipos, também serão identificadas as constantes no código Python para que sejam tratadas como constantes no código C, visando minimizar a criação de variáveis no código C gerado. Além disso, também serão tratadas as strings presentes nos programas em Python para que sejam convertidas para vetores de char no programa em C e, consequentemente, também serão identificadas quais são as variáveis que usam caracteres no programa Python que será forncecido na entrada para que sejam convertidas em variáveis do tipo char. 

Em relação às variáveis booleanas presentes em Python (variáveis que tenham como valor True ou False), estas serão convertidas para variáveis de tipos inteiros com valor 0 (para False) e 1 (para True). De forma análoga, serão convertidas as variáveis com valor None para ponteiros que apontam para NULL em C. 

Em relação às variavéis com valores numéricos, o sistema será capaz de convertê-las para os tipos *int* e *float*, logo ele não colocará variáveis do tipo *double* no código C de saída.   

**Operações Lógicas e Aritméticas**

O sistema será capaz de identificar as operações aritméticas básicas: adição (`+`), subtração (`-`), multiplicação (`*`), divisão (`/`), resto da divisão (`%`) e potenciação(`**`). O código Python pode conter operações aritméticas entre variáveis númericas de tipos diferentes (uma variável `float` sendo divida por uma variável `int`), assim o sistema irá usar, conjuntamente, o indetificador de variáveis para converter para a mesma operação em C usando variáveis do mesmo tipo. Quanto às operações lógicas, serão identificadas as três operações básicas com as palavras reservadas, `and`, `or` e `not`.

**Operações de Comparação**

O sistema identificará comparações em Python que usam os seguintes operadores: `<=`, `>=`, `!=`, `==`, `>`, `<`. Novamente, o código em C aprensentará os operandos com o mesmo tipo.

**Condicionais**

O sistema será capaz de identificar as comparações realizadas com: `if`, `else` e `elif`. Qualquer outra palavra reservada que esteja no código Python fornecido na entrada usada para comparação que não seja uma destas três não será identificada e será tratada como erro pelo sistema.

**Comandos de Repetição**

Em Python, há diferentes maneiras de criar laços de repetição. O sistema reconhecerá na entrada em Python três destes tipos de laços: `for` usado conjuntamente com a função `range` `for <variável> in range(<variável ou número inteiro>)`, `for` usado conjuntamente com uma lista `for <variável> in <lista>`, e o comando `while`. Se outros comandos além destes três estiverem presentes no código Python dado na entrada, serão considerados erro. 

**Estrutura de Dados**

Em Python existem várias estruturas de dados. O sistema reconhecerá aquelas que são as mais usadas nos códigos Python: _listas_, _tuplas_ e _dicionários_. 

Em relação às _listas_, ele será capaz de reconhecer apenas aquelas formadas por variáveis do mesmo tipo em Python. Apesar de Python aceitar listas com diferentes tipos e com diferentes estruturas, será reduzida às listas que, convertidas para C, tenham valores do mesmo tipo em todas as posições, para que o sistema seja factível e viável. Além disso, esses tipos devem ser: `int`, `float` ou `string`. 

As _tuplas_ seguem as mesmas restrições das listas, sendo ambas convertidas para listas ligadas de uma `struct` em C. 

Os _dicionários_ terão uma conversão para C semelhante às listas, diferenciando apenas no fato de que serão criados métodos em C para cada operação de recuperação de valor pela chave, ou atribuição de valor pela chave. Além disso, os dicionários serão convertidos para listas ligadas em C, sempre com um campo adicional indicando o respectivo valor daquela posição. 

**Funções Built-in em Python**

A linguagem Python oferece em sua biblioteca padrão uma série de funções já disponíveis pronta para uso e na maioria dos programa em Python algumas delas estão presentes. Assim selecionamos as que julgamos mais comuns e viáveis para serem convertidas e implementadas em C, que são: `range`, `xrange`, `append`, `insert`, `remove`, `print`, `del`, `open`, `close`, `split`, `read`, `write`, `len`, `min`, `max`, `input`. E ainda, como em C temos variáveis com tipos, também implementaremos a função `cast` para troca do tipo de uma variável. 

## 5. Critério de Aceitação

Para que o projeto seja considerado um sucesso, a aplicação deve ser capaz de, dentro dos limites de escopo pré-estabelecidos, converter corretamente um código escrito em Python para um código escrito em C. Para que a conversão seja considerada coerente, o código em C deve produzir a mesma saída que o código em Python, dada uma mesma entrada.
Além disso, é esperado que a aplicação consiga se recuprar de erros e apontá-los no código original, sugerindo eventuais correções.

## 6. Risco
Fatores de risco que podem comprometer o sucesso do projeto:

* Tratativa dos itens definidos no escopo do projeto ser mais complexa do que o esperado ou até impossíveis de serem abordadas com o ferramental utilizado no projeto.
* Estimativas de tempo erradas para histórias das *sprints* resultarem em atrasos que comprometam a entrega no prazo.
* Complexidade de trativas de conversão entre diferentes sistemas de memória, estático (C) e dinâmico(Python).

