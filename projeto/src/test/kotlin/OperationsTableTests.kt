package org.jetbrains.kotlin.testJunit.test

import model.ExpressionType
import model.OperationType
import org.junit.Assert
import org.junit.Test
import semanticAnalyzer.OperationsTable
import java.lang.Exception

class OperationsTableTests {

    @Test
    fun testIntToIntExpression() {
        val type = ExpressionType.INT()
        test(first = type, second = type, operationType = OperationType.PLUS, expectedExpressionType = ExpressionType.INT())
        test(first = type, second = type, operationType = OperationType.MINUS, expectedExpressionType = ExpressionType.INT())
        test(first = type, second = type, operationType = OperationType.MULTIPLICATION, expectedExpressionType = ExpressionType.INT())
        test(first = type, second = type, operationType = OperationType.DIVISION, expectedExpressionType = ExpressionType.INT())
        test(first = type, second = type, operationType = OperationType.MOD, expectedExpressionType = ExpressionType.INT())
        test(first = type, second = type, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.POW, expectedExpressionType = ExpressionType.INT())
    }

    @Test
    fun testDoubleToDoubleExpression() {
        val type = ExpressionType.DOUBLE()
        test(first = type, second = type, operationType = OperationType.PLUS, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = type, second = type, operationType = OperationType.MINUS, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = type, second = type, operationType = OperationType.MULTIPLICATION, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = type, second = type, operationType = OperationType.DIVISION, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = type, second = type, operationType = OperationType.MOD, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = type, second = type, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.POW, expectedExpressionType = ExpressionType.DOUBLE())
    }

    @Test
    fun testArrayToArrayExpression() {
        for (arrayType in ExpressionType.values) {
            val type = ExpressionType.ARRAY(arrayType)
            val secondType = ExpressionType.ARRAY(arrayType)
            val expectedType = ExpressionType.ARRAY(arrayType)
            test(first = type, second = secondType, operationType = OperationType.PLUS, expectedExpressionType = expectedType)
            test(first = type, second = secondType, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.BOOL())
            test(first = type, second = secondType, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.BOOL())
            test(first = type, second = secondType, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
            test(first = type, second = secondType, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        }
    }

    @Test
    fun testArrayToArrayDifferentTypes() {
        val arrayIntType = ExpressionType.ARRAY(ExpressionType.INT())
        val arrayDoubleType = ExpressionType.ARRAY(ExpressionType.DOUBLE())
        val arrayStringType = ExpressionType.ARRAY(ExpressionType.STRING())
        val arrayArrayDoubleType = ExpressionType.ARRAY(ExpressionType.ARRAY(ExpressionType.DOUBLE()))
        for (operationType in OperationType.values()) {
            // Estas operacoes sempre retornam um bool como expressionType, ou seja, nao vao dar diferença no teste
            if (operationType == OperationType.EQUAL || operationType == OperationType.NOT_EQUAL) {
                test(first = arrayIntType, second = arrayDoubleType, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
                test(first = arrayDoubleType, second = arrayIntType, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
                test(first = arrayDoubleType, second = arrayStringType, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
                test(first = arrayDoubleType, second = arrayArrayDoubleType, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
            } else {
                testIfExceptionIsTriggered(first = arrayIntType, second = arrayDoubleType, operationType = operationType)
                testIfExceptionIsTriggered(first = arrayDoubleType, second = arrayIntType, operationType = operationType)
                testIfExceptionIsTriggered(first = arrayDoubleType, second = arrayStringType, operationType = operationType)
                testIfExceptionIsTriggered(first = arrayDoubleType, second = arrayArrayDoubleType, operationType = operationType)
            }
        }
    }

    @Test
    fun testTupleToTupleExpression() {
        for (tupleType in ExpressionType.values) {
            val type = ExpressionType.TUPLE(tupleType)
            val secondType = ExpressionType.TUPLE(tupleType)
            val expectedType = ExpressionType.TUPLE(tupleType)
            test(first = type, second = secondType, operationType = OperationType.PLUS, expectedExpressionType = expectedType)
            test(first = type, second = secondType, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.BOOL())
            test(first = type, second = secondType, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.BOOL())
            test(first = type, second = secondType, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
            test(first = type, second = secondType, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        }
    }

    @Test
    fun testTupleToTupleDifferentTypes() {
        val tupleIntType = ExpressionType.TUPLE(ExpressionType.INT())
        val tupleDoubleType = ExpressionType.TUPLE(ExpressionType.DOUBLE())
        val tupleStringType = ExpressionType.TUPLE(ExpressionType.STRING())
        val tupleTupleDoubleType = ExpressionType.TUPLE(ExpressionType.TUPLE(ExpressionType.DOUBLE()))
        for (operationType in OperationType.values()) {
            // Estas operacoes sempre retornam um bool como expressionType, ou seja, nao vao dar diferença no teste
            if (operationType == OperationType.EQUAL || operationType == OperationType.NOT_EQUAL) {
                test(first = tupleIntType, second = tupleDoubleType, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
                test(first = tupleDoubleType, second = tupleIntType, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
                test(first = tupleDoubleType, second = tupleStringType, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
                test(first = tupleDoubleType, second = tupleTupleDoubleType, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
            } else {
                testIfExceptionIsTriggered(first = tupleIntType, second = tupleDoubleType, operationType = operationType)
                testIfExceptionIsTriggered(first = tupleDoubleType, second = tupleIntType, operationType = operationType)
                testIfExceptionIsTriggered(first = tupleDoubleType, second = tupleStringType, operationType = operationType)
                testIfExceptionIsTriggered(first = tupleDoubleType, second = tupleTupleDoubleType, operationType = operationType)
            }
        }
    }

    @Test
    fun testStringToStringExpression() {
        val type = ExpressionType.STRING()
        test(first = type, second = type, operationType = OperationType.PLUS, expectedExpressionType = ExpressionType.STRING())
        test(first = type, second = type, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
    }

    @Test
    fun testBoolToBoolExpression() {
        val type = ExpressionType.BOOL()
        test(first = type, second = type, operationType = OperationType.PLUS, expectedExpressionType = ExpressionType.INT())
        test(first = type, second = type, operationType = OperationType.MINUS, expectedExpressionType = ExpressionType.INT())
        test(first = type, second = type, operationType = OperationType.MULTIPLICATION, expectedExpressionType = ExpressionType.INT())
        test(first = type, second = type, operationType = OperationType.DIVISION, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = type, second = type, operationType = OperationType.MOD, expectedExpressionType = ExpressionType.INT())
        test(first = type, second = type, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.AND, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.OR, expectedExpressionType = ExpressionType.BOOL())
        test(first = type, second = type, operationType = OperationType.POW, expectedExpressionType = ExpressionType.INT())
    }

    @Test
    fun testIntToDoubleExpression() {
        intToDoubleExpression(invertFirstAndSecond = false)
    }

    @Test
    fun testDoubleToIntExpression() {
        intToDoubleExpression(invertFirstAndSecond = true)
    }

    private fun intToDoubleExpression(invertFirstAndSecond: Boolean) {
        val first = if (invertFirstAndSecond) ExpressionType.DOUBLE() else ExpressionType.INT()
        val second = if (invertFirstAndSecond) ExpressionType.INT() else ExpressionType.DOUBLE()
        test(first = first, second = second, operationType = OperationType.PLUS, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.MINUS, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.MULTIPLICATION, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.DIVISION, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.MOD, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.POW, expectedExpressionType = ExpressionType.DOUBLE())
    }

    @Test
    fun testIntToStringExpression() {
        intToStringExpression(invertFirstAndSecond = false)
    }

    @Test
    fun testStringToIntExpression() {
        intToStringExpression(invertFirstAndSecond = true)
    }

    private fun intToStringExpression(invertFirstAndSecond: Boolean) {
        val first = if (invertFirstAndSecond) ExpressionType.STRING() else ExpressionType.INT()
        val second = if (invertFirstAndSecond) ExpressionType.INT() else ExpressionType.STRING()
        test(first = first, second = second, operationType = OperationType.MULTIPLICATION, expectedExpressionType = ExpressionType.STRING())
    }

    @Test
    fun testIntToBoolExpression() {
        intToBoolExpression(invertFirstAndSecond = false)
    }

    @Test
    fun testBoolToIntExpression() {
        intToBoolExpression(invertFirstAndSecond = true)
    }

    private fun intToBoolExpression(invertFirstAndSecond: Boolean) {
        val first = if (invertFirstAndSecond) ExpressionType.BOOL() else ExpressionType.INT()
        val second = if (invertFirstAndSecond) ExpressionType.INT() else ExpressionType.BOOL()
        test(first = first, second = second, operationType = OperationType.PLUS, expectedExpressionType = ExpressionType.INT())
        test(first = first, second = second, operationType = OperationType.MINUS, expectedExpressionType = ExpressionType.INT())
        test(first = first, second = second, operationType = OperationType.DIVISION, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.MULTIPLICATION, expectedExpressionType = ExpressionType.INT())
        test(first = first, second = second, operationType = OperationType.MOD, expectedExpressionType = ExpressionType.INT())
        test(first = first, second = second, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.POW, expectedExpressionType = ExpressionType.INT())
    }

    @Test
    fun testDoubleToBoolExpression() {
        doubleToBoolExpression(invertFirstAndSecond = false)
    }

    @Test
    fun testBoolToDoubleExpression() {
        doubleToBoolExpression(invertFirstAndSecond = true)
    }

    private fun doubleToBoolExpression(invertFirstAndSecond: Boolean) {
        val first = if (invertFirstAndSecond) ExpressionType.BOOL() else ExpressionType.DOUBLE()
        val second = if (invertFirstAndSecond) ExpressionType.DOUBLE() else ExpressionType.BOOL()
        test(first = first, second = second, operationType = OperationType.PLUS, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.MINUS, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.DIVISION, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.MULTIPLICATION, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.MOD, expectedExpressionType = ExpressionType.DOUBLE())
        test(first = first, second = second, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.BOOL())
        test(first = first, second = second, operationType = OperationType.POW, expectedExpressionType = ExpressionType.DOUBLE())
    }

    @Test
    fun testStringToBoolExpression() {
        stringToBoolExpression(invertFirstAndSecond = false)
    }

    @Test
    fun testBoolToStringExpression() {
        stringToBoolExpression(invertFirstAndSecond = true)
    }

    private fun stringToBoolExpression(invertFirstAndSecond: Boolean) {
        val first = if (invertFirstAndSecond) ExpressionType.BOOL() else ExpressionType.STRING()
        val second = if (invertFirstAndSecond) ExpressionType.STRING() else ExpressionType.BOOL()
        test(first = first, second = second, operationType = OperationType.MULTIPLICATION, expectedExpressionType = ExpressionType.STRING())
    }

    @Test
    fun testArrayToOtherExpression() {
        arrayToOtherExpression(invertFirstAndSecond = false)
    }

    @Test
    fun testOtherToArrayExpression() {
        arrayToOtherExpression(invertFirstAndSecond = true)
    }

    private fun arrayToOtherExpression(invertFirstAndSecond: Boolean) {
        val arrayType = ExpressionType.ARRAY(ExpressionType.INT())
        val otherType = ExpressionType.INT()
        val first = if (invertFirstAndSecond) arrayType else otherType
        val second = if (invertFirstAndSecond) otherType else arrayType
        for (operationType in OperationType.values()) {
            if (operationType == OperationType.MULTIPLICATION) {
                test(first = first, second = second, operationType = operationType, expectedExpressionType = arrayType)
            } else if (operationType == OperationType.EQUAL || operationType == OperationType.NOT_EQUAL) {
                test(first = first, second = second, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
            } else {
                testIfExceptionIsTriggered(first = first, second = second, operationType = operationType)
            }
        }
    }

    @Test
    fun testTupleToOtherExpression() {
        tupleToOtherExpression(invertFirstAndSecond = false)
    }

    @Test
    fun testOtherToTupleExpression() {
        tupleToOtherExpression(invertFirstAndSecond = true)
    }

    private fun tupleToOtherExpression(invertFirstAndSecond: Boolean) {
        val tupleType = ExpressionType.TUPLE(ExpressionType.INT())
        val otherType = ExpressionType.INT()
        val first = if (invertFirstAndSecond) tupleType else otherType
        val second = if (invertFirstAndSecond) otherType else tupleType
        for (operationType in OperationType.values()) {
            if (operationType == OperationType.MULTIPLICATION) {
                test(first = first, second = second, operationType = operationType, expectedExpressionType = tupleType)
            } else if (operationType == OperationType.EQUAL || operationType == OperationType.NOT_EQUAL) {
                test(first = first, second = second, operationType = operationType, expectedExpressionType = ExpressionType.BOOL())
            } else {
                testIfExceptionIsTriggered(first = first, second = second, operationType = operationType)
            }
        }
    }

    private fun errorToOtherTypeExpression(invertFirstAndSecond: Boolean, otherExpressionType: ExpressionType) {
        val first = if (invertFirstAndSecond) ExpressionType.ERROR() else otherExpressionType
        val second = if (invertFirstAndSecond) otherExpressionType else ExpressionType.ERROR()
        test(first = first, second = second, operationType = OperationType.PLUS, expectedExpressionType = ExpressionType.ERROR())
        test(first = first, second = second, operationType = OperationType.MINUS, expectedExpressionType = ExpressionType.ERROR())
        test(first = first, second = second, operationType = OperationType.DIVISION, expectedExpressionType = ExpressionType.ERROR())
        test(first = first, second = second, operationType = OperationType.MULTIPLICATION, expectedExpressionType = ExpressionType.ERROR())
        test(first = first, second = second, operationType = OperationType.MOD, expectedExpressionType = ExpressionType.ERROR())
        test(first = first, second = second, operationType = OperationType.GREATER, expectedExpressionType = ExpressionType.ERROR())
        test(first = first, second = second, operationType = OperationType.LOWER, expectedExpressionType = ExpressionType.ERROR())
        test(first = first, second = second, operationType = OperationType.GREATER_EQUAL, expectedExpressionType = ExpressionType.ERROR())
        test(first = first, second = second, operationType = OperationType.LOWER_EQUAL, expectedExpressionType = ExpressionType.ERROR())
    }

    fun errorToEveryTypeExpression() {

        for (otherExpressionType in ExpressionType.values) {
            errorToOtherTypeExpression(invertFirstAndSecond = false, otherExpressionType = otherExpressionType)
        }

    }

    fun everyTypeToErrorExpression() {
        for (otherExpressionType in ExpressionType.values) {
            errorToOtherTypeExpression(invertFirstAndSecond = true, otherExpressionType = otherExpressionType)
        }
    }

    @Test
    fun testErrorToEveryTypeExpression() {
        errorToEveryTypeExpression()
    }

    @Test
    fun testEveryTypeToErrorExpression() {
        everyTypeToErrorExpression()
    }

    @Test
    fun testIndependentOperations() {
        testAllExpressionsCombinations(operationType = OperationType.EQUAL)
        testAllExpressionsCombinations(operationType = OperationType.NOT_EQUAL)
    }
    
    private fun testAllExpressionsCombinations(operationType: OperationType) {
        for (first in ExpressionType.values) {
            for (second in ExpressionType.values) {
                val expectedExpressionType = if (first == ExpressionType.ERROR() || second == ExpressionType.ERROR()) ExpressionType.ERROR() else ExpressionType.BOOL()
                test(first = first, second = second, operationType = operationType, expectedExpressionType = expectedExpressionType)
            }
        }
    }

    private fun test(first: ExpressionType, second: ExpressionType, operationType: OperationType, expectedExpressionType: ExpressionType) {
        val expressionType = OperationsTable.expressionType(firstExpressionType = first,
                                                            secondExpressionType = second,
                                                            operationType = operationType)
        Assert.assertEquals(expectedExpressionType, expressionType)
    }

    private fun testIfExceptionIsTriggered(first: ExpressionType, second: ExpressionType, operationType: OperationType) {
        try {
            val expressionType = OperationsTable.expressionType(firstExpressionType = first,
                secondExpressionType = second,
                operationType = operationType
            )
            Assert.fail("As duas expressões e a operação deveriam retornar uma exception, mas retornaram a ExpressionType " + expressionType.name)
        } catch(exception: Exception) {
            Assert.assertTrue(true) // sucess
        }
    }
}