package org.jetbrains.kotlin.testJunit.test.auxiliaryClasses

import errorHandler.*
import helper.FormatterHelper
import java.lang.Exception

class MockErrorHandler: ErrorHandler {
    var errorMessage = ""

    override fun handleError(error: Error) {
        errorMessage = error.localizedMessage.replace("\\\\", "\\")
    }

    override fun handleException(exception: Exception) {
        if (exception is parser.ParseException) {
            val foundToken = exception.currentToken.next
            val foundTokenType = exception.tokenImage[foundToken.kind]
            val foundTokenImage = FormatterHelper.add_escapes(foundToken.image)

            val expectedTokens = exception.expectedTokenSequences
                .fold("") { expectedTokens, image ->
                    expectedTokens + "\n${exception.tokenImage[image[0]]}"
                }

            errorMessage = "Error: Found $foundTokenType with image: \"$foundTokenImage\" " +
                "at line ${foundToken.beginLine}, column ${foundToken.beginColumn}.\n" +
                "Was expecting one of the following tokens:$expectedTokens"

        } else {
            val messageWithEscapes = FormatterHelper.add_escapes(exception.localizedMessage)
            errorMessage = messageWithEscapes ?: ""
        }
    }

    override fun exceptionList(): ArrayList<Exception> {
        return ArrayList()
    }
}