package org.jetbrains.kotlin.testJunit.test.auxiliaryClasses

import conversor.Conversor
import model.*

class MockConversor: Conversor {

    var functionCalls = arrayOf<String>()

    override fun addVarAssignment(variable: Variable, expression: Expression) {
        functionCalls += ConversorMethods.addVarAssignment.name
    }

    override fun verifyTupleType(expression: Expression):String {
        functionCalls += ConversorMethods.verifyTupleType.name
        return ""
    }
    
    override fun getSizeTuple(tuple: Tuple): String {
        functionCalls += ConversorMethods.getSizeTuple.name
        return ""
    }

    override fun verifyListType(expression: Expression):String {
        functionCalls += ConversorMethods.verifyListType.name
        return ""
    }

    override fun getSizeList(list: model.List): String {
        functionCalls += ConversorMethods.getSizeList.name
        return ""
    }
    override fun generateFile() {
        functionCalls += ConversorMethods.generateFile.name
    }

    override fun startIfDeclaration(condition: Expression) {
        functionCalls += ConversorMethods.startIfDeclaration.name
    }

    override fun startElifDeclaration(condition: Expression) {
        functionCalls += ConversorMethods.startElifDeclaration.name
    }

    override fun startElseDeclaration() {
        functionCalls += ConversorMethods.startElseDeclaration.name
    }

    override fun endIfDeclaration() {
        functionCalls += ConversorMethods.endIfDeclaration.name
    }

    override fun startForDeclaration(variable: Variable, iterableElement: Expression) {
        functionCalls += ConversorMethods.startForDeclaration.name
    }

    override fun endForDeclaration() {
        functionCalls += ConversorMethods.endForDeclaration.name
    }

    override fun startWhileDeclaration(condition: Expression) {
        functionCalls += ConversorMethods.startWhileDeclaration.name
    }

    override fun endWhileDeclaration() {
        functionCalls += ConversorMethods.endWhileDeclaration.name
    }
}

enum class ConversorMethods {
    addVarAssignment,
    startIfDeclaration,
    startElifDeclaration,
    startElseDeclaration,
    endIfDeclaration,
    generateFile,
    startForDeclaration,
    endForDeclaration,
    startWhileDeclaration,
    endWhileDeclaration,
    verifyTupleType,
    getSizeTuple,
    verifyListType,
    getSizeList
}












