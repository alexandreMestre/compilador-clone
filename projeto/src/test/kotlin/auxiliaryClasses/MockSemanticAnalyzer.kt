package org.jetbrains.kotlin.testJunit.test.auxiliaryClasses

import model.*
import semanticAnalyzer.*

class MockSemanticAnalyzer: SemanticAnalyzer {

    var functionCalls = arrayOf<String>()

    override fun lookUpSymbol(ofName: String): Symbol? {
        functionCalls += SemanticAnalyzerMethods.lookUpSymbol.name + "_" + ofName
        return ConcreteVariable(
            expressionType = ExpressionType.STRING(), value = "abc", name = "name",
            declarationsInCurrentScope = 0, isNewDeclaration = false
        )
    }

    override fun createVariable(ofName: String, expressionType: ExpressionType, value: String?): Symbol {
        functionCalls += SemanticAnalyzerMethods.createVariable.name + "_" + ofName
        return ConcreteVariable(
            expressionType = ExpressionType.STRING(), value = "abc", name = "name",
            declarationsInCurrentScope = 0, isNewDeclaration = false
        )
    }
    override fun createMethod(ofName: String, expressionType: ExpressionType, value: String?): Symbol {
        functionCalls += SemanticAnalyzerMethods.createMethod.name + "_" + ofName
        return ConcreteVariable(
            expressionType = ExpressionType.STRING(), value = "abc", name = "name",
            declarationsInCurrentScope = 0, isNewDeclaration = false
        )
    }
    override fun createConstant(ofValue: String?, expressionType: ExpressionType): Symbol {
        functionCalls += SemanticAnalyzerMethods.createConstant.name + "_" + ofValue
        return ConcreteVariable(
            expressionType = ExpressionType.STRING(), value = "abc", name = "name",
            declarationsInCurrentScope = 0, isNewDeclaration = false
        )
    }

    override fun createList(ofExpressions: ArrayList<Expression>, value: String?): Symbol {
        functionCalls += SemanticAnalyzerMethods.createList.name + "_" + value
        return ConcreteList(
            expressions = ArrayList(),
            expressionType = ExpressionType.ARRAY(ExpressionType.DOUBLE()),
            value = "abc"
        )
    }

    override fun createTuple(ofExpressions: ArrayList<Expression>, value: String?): Symbol {
        functionCalls += SemanticAnalyzerMethods.createTuple.name + "_" + value
        return ConcreteTuple(
            expressions = ArrayList(),
            expressionType = ExpressionType.TUPLE(ExpressionType.DOUBLE()),
            value = "abc"
        )
    }

    override fun createOperation(ofType: OperationType, firstExpression: Expression, secondExpression: Expression): Operation {
        functionCalls += SemanticAnalyzerMethods.createOperation.name + "_" + ofType.name
        return ConcreteOperation(OperationType.PLUS, firstExpression, secondExpression, ExpressionType.INT())
    }

    override fun createOperation(ofType: OperationType, expression: Expression): Operation {
        functionCalls += SemanticAnalyzerMethods.createOperation.name + "_" + ofType.name
        return ConcreteOperation(OperationType.NOT, expression, null, ExpressionType.INT())
    }

    override fun getIterableExpressionSubtype(expression: Expression): ExpressionType {
        functionCalls += SemanticAnalyzerMethods.getIterableExpressionSubtype.name
        return ExpressionType.STRING()
    }

    override fun decrementScope() {
        functionCalls += SemanticAnalyzerMethods.decrementScope.name
    }

    override fun incrementScope() {
        functionCalls += SemanticAnalyzerMethods.incrementScope.name
    }
}

enum class SemanticAnalyzerMethods {
    lookUpSymbol,
    ensureIterable,
    createVariable,
    createMethod,
    createList,
    createTuple,
    createConstant,
    createOperation,
    getIterableExpressionSubtype,
    decrementScope,
    incrementScope
}