package org.jetbrains.kotlin.testJunit.test

import model.*
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import semanticAnalyzer.ConcreteSemanticAnalyzer
import semanticAnalyzer.SemanticAnalyzer
import semanticAnalyzer.SymbolTable

class SemanticAnalyzerTests {

    class MockSymbolTable : SymbolTable {
        var addSymbolWasCalled = false
        var removeSymbolWasCalled = false
        var lookUpVariableWasCalled = false
        var lookUpMethodWasCalled = false
        var lookUpSymbolWasCalled = false
        var decrementScopeWasCalled = false
        var incrementScopeWasCalled = false

        override fun add(symbol: Symbol): Symbol {
            addSymbolWasCalled = true
            return ConcreteVariable(
                expressionType = ExpressionType.STRING(), value = "abc", name = "name",
                declarationsInCurrentScope = 0, isNewDeclaration = false
            )
        }

        override fun remove(symbol: Symbol) {
            removeSymbolWasCalled = true
        }

        override fun lookUpVariable(name: String): Variable? {
            lookUpVariableWasCalled = true
            return null
        }

        override fun lookUpMethod(name: String): Method? {
            lookUpMethodWasCalled = true
            return null
        }

        override fun lookUpSymbol(ofName: String): Symbol? {
            lookUpSymbolWasCalled = true
            return null
        }

        override fun decrementScope() {
            decrementScopeWasCalled = true
        }

        override fun incrementScope() {
            incrementScopeWasCalled = true
        }
    }

    private var symbolTable: MockSymbolTable = MockSymbolTable()
    private var semanticAnalyzer: SemanticAnalyzer = ConcreteSemanticAnalyzer(symbolTable = MockSymbolTable())

    @Before
    fun setup() {
        symbolTable = MockSymbolTable()
        semanticAnalyzer = ConcreteSemanticAnalyzer(symbolTable = symbolTable)
    }

    @Test
    fun testLookUpSymbol() {
        Assert.assertFalse(symbolTable.lookUpSymbolWasCalled)
        semanticAnalyzer.lookUpSymbol(ofName = "name")
        Assert.assertTrue(symbolTable.lookUpSymbolWasCalled)
    }

    @Test
    fun testCreateVariable() {
        Assert.assertFalse(symbolTable.addSymbolWasCalled)
        semanticAnalyzer.createVariable(ofName = "Variable", expressionType = ExpressionType.BOOL(), value = null)
        Assert.assertTrue(symbolTable.addSymbolWasCalled)
    }

    @Test
    fun testCreateMethod() {
        Assert.assertFalse(symbolTable.addSymbolWasCalled)
        semanticAnalyzer.createMethod(ofName = "Variable", expressionType = ExpressionType.BOOL(), value = null)
        Assert.assertTrue(symbolTable.addSymbolWasCalled)
    }

    @Test
    fun testCreateConstant() {
        Assert.assertFalse(symbolTable.addSymbolWasCalled)
        semanticAnalyzer.createConstant(ofValue = "Value", expressionType = ExpressionType.BOOL())
        Assert.assertTrue(symbolTable.addSymbolWasCalled)
    }

    @Test
    fun testCreateList() {
        Assert.assertFalse(symbolTable.addSymbolWasCalled)
        semanticAnalyzer.createList(ofExpressions = ArrayList<Expression>(), value = "Value")
        Assert.assertTrue(symbolTable.addSymbolWasCalled)
    }

    @Test
    fun testIncrementScope() {
        Assert.assertFalse(symbolTable.incrementScopeWasCalled)
        semanticAnalyzer.incrementScope()
        Assert.assertTrue(symbolTable.incrementScopeWasCalled)
    }

    @Test
    fun testDecrementScope() {
        Assert.assertFalse(symbolTable.decrementScopeWasCalled)
        semanticAnalyzer.decrementScope()
        Assert.assertTrue(symbolTable.decrementScopeWasCalled)
    }
}