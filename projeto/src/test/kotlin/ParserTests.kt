package org.jetbrains.kotlin.testJunit.test

import helper.ConcreteIndentationHelper
import org.jetbrains.kotlin.testJunit.test.auxiliaryClasses.MockSemanticAnalyzer
import org.jetbrains.kotlin.testJunit.test.auxiliaryClasses.MockConversor
import org.jetbrains.kotlin.testJunit.test.auxiliaryClasses.MockErrorHandler
import org.junit.Assert
import org.junit.Test
import java.io.File
import parser.Compiler

class ParserTests {
    @Test
    fun parserTest() {

        // carrega os diretórios com os arquivos de testes do parser
        val inputFolder = File("src/test/kotlin/resources/parserTest/input")
        val outputFolder = File("src/test/kotlin/resources/parserTest/output")

        // percorre todos os testes do diretório
        inputFolder.walk().filter { it.isFile }.forEach {
            // carrega a input stream do arquivo
            val inputStream = it.inputStream()

            // cria classes mocks para testar apenas o parser
            val mockSemanticAnalyzer = MockSemanticAnalyzer()
            val mockConversor = MockConversor()
            val mockErrorHandler = MockErrorHandler()
            val indentationHelper = ConcreteIndentationHelper()

            // cria uma instância do compilador com o arquivo de teste como entrada
            val compiler = Compiler(inputStream, mockSemanticAnalyzer,
                                    mockConversor, mockErrorHandler, indentationHelper)

            // executa o compilador
            compiler.execute()

            // pega o arquivo de saída correspondente ao teste
            val pathFromTestFolder = it.toRelativeString(inputFolder).replace(".py", ".txt")
            val outputFilePath = "${outputFolder.absolutePath}/$pathFromTestFolder"
            val outputFile = File(outputFilePath)

            // lê do arquivo de saída as chamadas esperadas
            val expectedCalls = outputFile.readLines()

            // formata o resultado do teste para comparar com o arquivo de saida
            var compilerCalls = mutableListOf<String>()
            compilerCalls.add("Métodos do analisador semântico:")
            for (methodCall in mockSemanticAnalyzer.functionCalls) {
                compilerCalls.add(methodCall)
            }
            compilerCalls.add("Métodos do conversor:")
            for (methodCall in mockConversor.functionCalls) {
                compilerCalls.add(methodCall)
            }

            // cria a mensagem de erro base, informando o arquivo em que o teste falhou
            val errorMessage = "parser test: $pathFromTestFolder\n"

            // verifica se é um teste de entrada válida ou não
            if (pathFromTestFolder.startsWith("failure/")) {
                // carrega a mensagem de erro esperada e compara com a mensagem de erro obtida
                val expectedErrorMessage = outputFile.readText()
                Assert.assertEquals(errorMessage + "Error message doesn't match",
                                    expectedErrorMessage, mockErrorHandler.errorMessage)
            } else {
                // verifica se as chamadas estão corretas
                Assert.assertEquals(errorMessage + "Function calls doesn't match",
                    expectedCalls, compilerCalls)
            }
        }
    }
}