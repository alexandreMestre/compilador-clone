package org.jetbrains.kotlin.testJunit.test

import org.junit.Assert
import org.junit.Test
import semanticAnalyzer.ConcreteSymbolTable
import model.ConcreteVariable
import model.ExpressionType
import model.Variable

class SymbolTableTests {

    // Checa se as propriedades da variável criada como uma nova entrada na tabela estão corretas. Para que esteja
    // correto é necessário que a variável seja uma nova declaração e seja a primeira no escopo atual
    private fun isNewVariableEntryCorrect(variable: Variable): Boolean {
        return variable.isNewDeclaration && variable.declarationsInCurrentScope == 1
    }

    // Checa se as estão corretas as propriedades da variável criada como um update de valor de outra variável, com mesmo
    // nome, do escopo atual. Para que esteja correto é necessário que a variável não seja uma nova declaração.
    // É importante notar que o valor declarationsInCurrentScope não pode ser checado pois a nova variável, com valor
    // alterado e com mesmo tipo da anterior no escopo atual, terá o mesmo número de declarações da variável anterior,
    // que não é conhecido nessa função.
    private fun isUpdatedVariableCorrect(variable: Variable): Boolean {
        return !variable.isNewDeclaration
    }

    // Checa se as estão corretas as propriedades da variável criada como uma nova declaração de outra variável, com
    // mesmo nome, do escopo atual. Para que esteja correto é necessário que a variável seja uma nova declaração, pois,
    // apesar de ter um nome já presente no escopo atual, ela tem outro tipo e portanto conta como uma nova declaração.
    // Além disso o contador de declarações no escopo atual deve ser maior que um, pois se fosse 1, essa variável estaria
    // sendo adicionada pela primeira vez no escopo atual e não seria uma nova declaração de um nome já existente.
    private fun isNewDeclarationOfVariableCorrect(variable: Variable): Boolean {
        return variable.isNewDeclaration && variable.declarationsInCurrentScope > 1
    }

    private fun areVariablesPropertiesMatching(firstVariable: Variable, secondVariable: Variable): Boolean {
        return firstVariable.name == secondVariable.name && firstVariable.value == secondVariable.value &&
            firstVariable.expressionType == secondVariable.expressionType
    }

    // Nesse teste uma variável é inserida na tabela. A variável retornada não deve ser nula e deve ter as
    // propriedades de uma variável recém adicionada
    @Test
    fun testAddNewVariable() {
        val symbolTable = ConcreteSymbolTable()
        val variable = ConcreteVariable(ExpressionType.STRING(), value = "x", name = "temp")
        val addedVariable = symbolTable.add(variable) as Variable
        Assert.assertNotNull(addedVariable)
        Assert.assertTrue(isNewVariableEntryCorrect(addedVariable))
    }

    // Nesse teste duas variáveis com mesmo nome e tipos são inseridas na tabela no mesmo escopo. As variáveis
    // retornadas não devem ser nulas. A primeira variável retornada deve ter as propriedades de uma variável
    // recém adicionada e a segunda deve ter as propriedades de uma variável que foi atualizada
    @Test
    fun testAddTwoVariablesOfSameTypeOnCurrentScope() {
        val symbolTable = ConcreteSymbolTable()
        val firstVariable = ConcreteVariable(ExpressionType.STRING(), value = "x", name = "temp")
        val secondVariable = ConcreteVariable(ExpressionType.STRING(), value = "y", name = "temp")
        val firstAddedVariable = symbolTable.add(firstVariable) as Variable
        val secondAddedVariable = symbolTable.add(secondVariable) as Variable
        Assert.assertNotNull(firstAddedVariable)
        Assert.assertNotNull(secondAddedVariable)
        Assert.assertTrue(isNewVariableEntryCorrect(firstAddedVariable))
        Assert.assertEquals(firstAddedVariable.value, "x")
        Assert.assertTrue(isUpdatedVariableCorrect(secondAddedVariable))
        Assert.assertEquals(secondAddedVariable.value, "y")
    }

    // Nesse teste duas variáveis com mesmo nome e tipos diferentes são inseridas na tabela no mesmo escopo.
    // As variáveis retornadas não devem ser nulas. A primeira variável retornada deve ter as propriedades de
    // uma variável recém adicionada e a segunda deve ter as propriedades de uma variável que foi redeclarada.
    // Os tipos e valores das variáveis adicionadas tem que ser igual ao das variáveis retornadas
    @Test
    fun testAddTwoVariablesOfDifferentTypesOnCurrentScope() {
        val symbolTable = ConcreteSymbolTable()
        val firstVariable = ConcreteVariable(ExpressionType.STRING(), value = "x", name = "temp")
        val secondVariable = ConcreteVariable(ExpressionType.INT(), value = "2", name = "temp")
        val firstAddedVariable = symbolTable.add(firstVariable) as Variable
        val secondAddedVariable = symbolTable.add(secondVariable) as Variable
        Assert.assertNotNull(firstAddedVariable)
        Assert.assertNotNull(secondAddedVariable)
        Assert.assertTrue(isNewVariableEntryCorrect(firstAddedVariable))
        Assert.assertEquals(firstAddedVariable.expressionType, ExpressionType.STRING())
        Assert.assertEquals(firstAddedVariable.value, "x")
        Assert.assertTrue(isNewDeclarationOfVariableCorrect(secondAddedVariable))
        Assert.assertEquals(secondAddedVariable.expressionType, ExpressionType.INT())
        Assert.assertEquals(secondAddedVariable.value, "2")
    }

    // Nesse teste duas variáveis com mesmo nome e tipos diferentes são inseridas na tabela em escopos diferentes.
    // As variáveis retornadas não devem ser nulas. A primeira variável retornada deve ter as propriedades de
    // uma variável recém adicionada e a segunda também, pois é como se esconde-se a variável do escopo anterior.
    // Os tipos e valores das variáveis adicionadas tem que ser igual ao das variáveis retornadas
    @Test
    fun testAddTwoVariablesOnDifferentScopes() {
        val symbolTable = ConcreteSymbolTable()
        val firstVariable = ConcreteVariable(ExpressionType.STRING(), value = "x", name = "temp")
        val secondVariable = ConcreteVariable(ExpressionType.INT(), value = "2", name = "temp")
        val firstAddedVariable = symbolTable.add(firstVariable) as Variable
        symbolTable.incrementScope()
        val secondAddedVariable = symbolTable.add(secondVariable) as Variable
        Assert.assertNotNull(firstAddedVariable)
        Assert.assertNotNull(secondAddedVariable)
        Assert.assertTrue(isNewVariableEntryCorrect(firstAddedVariable))
        Assert.assertEquals(firstAddedVariable.expressionType, ExpressionType.STRING())
        Assert.assertEquals(firstAddedVariable.value, "x")
        Assert.assertTrue(isNewVariableEntryCorrect(secondAddedVariable))
        Assert.assertEquals(secondAddedVariable.expressionType, ExpressionType.INT())
        Assert.assertEquals(secondAddedVariable.value, "2")
    }

    // Nesse teste duas variáveis com mesmo nome e tipos iguais são inseridas na tabela em escopos diferentes.
    // As variáveis retornadas não devem ser nulas. A primeira variável retornada deve ter as propriedades de
    // uma variável recém adicionada e a segunda não, pois esta esta é igual a primeira só que com valor possivelmente
    // alterado.
    @Test
    fun testUpdateVariableOnDifferentScopes() {
        val symbolTable = ConcreteSymbolTable()
        val firstVariable = ConcreteVariable(ExpressionType.INT(), value = "5", name = "temp")
        val secondVariable = ConcreteVariable(ExpressionType.INT(), value = "2", name = "temp")
        val firstAddedVariable = symbolTable.add(firstVariable) as Variable
        symbolTable.incrementScope()
        val secondAddedVariable = symbolTable.add(secondVariable) as Variable
        Assert.assertNotNull(firstAddedVariable)
        Assert.assertNotNull(secondAddedVariable)
        Assert.assertTrue(isNewVariableEntryCorrect(firstAddedVariable))
        Assert.assertEquals(firstAddedVariable.expressionType, ExpressionType.INT())
        Assert.assertEquals(firstAddedVariable.value, "5")
        Assert.assertTrue(isUpdatedVariableCorrect(secondAddedVariable))
        Assert.assertEquals(secondAddedVariable.expressionType, ExpressionType.INT())
        Assert.assertEquals(secondAddedVariable.value, "2")
        Assert.assertEquals(firstAddedVariable.declarationsInCurrentScope, secondAddedVariable.declarationsInCurrentScope)
    }

    // Nesse teste uma variável é inserida na tabela. Em seguida é procurada na tabela pelo nome. A variável
    // adicionada deve ser igual a retornada na busca e a retornada na busca não deve ser uma nova declaração
    // pois a operação realizada foi de busca e não adição
    @Test
    fun testLookupVariable() {
        val symbolTable = ConcreteSymbolTable()
        val firstVariable = ConcreteVariable(ExpressionType.STRING(), value = "x", name = "temp")
        symbolTable.add(firstVariable)
        val lookedVariable = symbolTable.lookUpVariable("temp") as Variable
        Assert.assertTrue(areVariablesPropertiesMatching(firstVariable, lookedVariable))
        Assert.assertFalse(lookedVariable.isNewDeclaration)
    }

    // Nesse teste é feita a busca por uma variável que não está presente na tabela. A variável retornada
    // deve ser nula
    @Test
    fun testLookupNotPresentVariable() {
        val symbolTable = ConcreteSymbolTable()
        val lookedVariable = symbolTable.lookUpVariable("temp")
        Assert.assertNull(lookedVariable)
    }

    // Nesse teste uma variável é adicionada em um escopo e outra em um novo escopo, ambas com mesmo nome.
    // É feita uma busca pelo nome no novo escopo e em seguida o escopo anterior é restaurado. Uma nova busca
    // pelo mesmo nome é efetuada. As variáveis adicionadas não devem ser nulas e ambas devem ser tratadas como
    // novas entradas na tabala. É feita uma checagem se os valores e os tipos das variáveis adicionadas estão
    // corretos e se as variáveis buscadas estão corretas
    @Test
    fun testScopeWithVariableRestoration() {
        val symbolTable = ConcreteSymbolTable()
        val firstVariable = ConcreteVariable(ExpressionType.STRING(), value = "x", name = "temp")
        val secondVariable = ConcreteVariable(ExpressionType.INT(), value = "2", name = "temp")
        val firstAddedVariable = symbolTable.add(firstVariable) as Variable
        symbolTable.incrementScope()
        val secondAddedVariable = symbolTable.add(secondVariable) as Variable
        val newScopeLookedUpVariable = symbolTable.lookUpVariable("temp") as Variable
        symbolTable.decrementScope()
        val oldScopeLookedUpVariable = symbolTable.lookUpVariable("temp") as Variable

        Assert.assertNotNull(firstAddedVariable)
        Assert.assertNotNull(secondAddedVariable)
        Assert.assertTrue(isNewVariableEntryCorrect(firstAddedVariable))
        Assert.assertEquals(firstAddedVariable.expressionType, ExpressionType.STRING())
        Assert.assertEquals(firstAddedVariable.value, "x")
        Assert.assertTrue(isNewVariableEntryCorrect(secondAddedVariable))
        Assert.assertEquals(secondAddedVariable.expressionType, ExpressionType.INT())
        Assert.assertEquals(secondAddedVariable.value, "2")
        Assert.assertTrue(areVariablesPropertiesMatching(secondVariable, newScopeLookedUpVariable))
        Assert.assertTrue(areVariablesPropertiesMatching(firstVariable, oldScopeLookedUpVariable))
        Assert.assertFalse(areVariablesPropertiesMatching(firstVariable, newScopeLookedUpVariable))
        Assert.assertFalse(areVariablesPropertiesMatching(secondVariable, oldScopeLookedUpVariable))
    }

    // Nesse teste uma variável é adicionada na tabela e o escopo é incrementado. É feita uma busca pelo
    // nome e o escopo anterior é restaurado. Em seguida uma nova busca é realizada. A variável adicionada e
    // as retornadas nas buscas não devem se nulas. É feita uma checagem se as variáveis buscadas são iguais
    // a adicionada.
    @Test
    fun testScopeWithoutVariableRestoration() {
        val symbolTable = ConcreteSymbolTable()
        val firstVariable = ConcreteVariable(ExpressionType.STRING(), value = "x", name = "temp")
        val firstAddedVariable = symbolTable.add(firstVariable) as Variable
        symbolTable.incrementScope()
        val newScopeLookedUpVariable = symbolTable.lookUpVariable("temp") as Variable
        symbolTable.decrementScope()
        val oldScopeLookedUpVariable = symbolTable.lookUpVariable("temp") as Variable

        Assert.assertNotNull(firstAddedVariable)
        Assert.assertNotNull(newScopeLookedUpVariable)
        Assert.assertNotNull(oldScopeLookedUpVariable)
        Assert.assertTrue(areVariablesPropertiesMatching(firstVariable, oldScopeLookedUpVariable))
        Assert.assertTrue(areVariablesPropertiesMatching(firstVariable, newScopeLookedUpVariable))
    }
}