#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "data_structs.h"


int lenListInt(ListInt *list) { 

	ListInt *p = list; 
	int tam = 0; 
	while (p != NULL) { 
		p = p->next;
		tam++;
	}

	return tam;
}

int LowerEqualListInt(ListInt *listA, ListInt *listB) { 

	int tamA = lenListInt(listA);
	int tamB = lenListInt(listB);

	if(tamA <= tamB) { 
		return 1; 
	} else { 
		return 0;
	}

	return 0;

}

int GreaterEqualListInt(ListInt *listA, ListInt *listB) { 

	int tamA = lenListInt(listA);
	int tamB = lenListInt(listB);

	if(tamA >= tamB) { 
		return 1; 
	} else { 
		return 0;
	}

	return 0;

}

int EqualListInt(ListInt *listA, ListInt *listB) { 

	int tamA = lenListInt(listA);
	int tamB = lenListInt(listB);

	if(tamA == tamB) { 
		ListInt *p = listA;
		ListInt *q = listB;

		while(q != NULL && p != NULL) { 
			if(q->value == p->value) { 
				q = q->next;
				p = p->next;
			}
			else { 
				return 0;
			}

		}
	}
	else { 
		return 0; 
	}
	return 1; 
}

int *getArrayfromListInt(ListInt *list) { 

	int tam = lenListInt(list); 
	int *array; 

	array = malloc(tam*sizeof(int)); 
	int i = 0; 
	ListInt *p = list; 

	while(p != NULL) { 
		array[i] = p->value; 
		p = p->next; 
		i++; 
	}

	return array; 
}

int MaxListInt(ListInt *list) { 
	int max = list->value;
	ListInt *p = list; 
	while(p != NULL) { 
		if(p->value > max) { 
			max = p->value;
		}	
		p = p->next; 
	}
	return max; 
}

int MinListInt(ListInt *list) { 
	int min = list->value; 
	ListInt *p = list; 
	while (p != NULL) { 
		if(p->value < min) { 
			min = p->value; 
		}
		p = p->next;
	}
	return min;
}

ListInt *ConcatenateListInt(ListInt *listA, ListInt *listB) {

	ListInt *p = NULL;
	ListInt *q = listA; 
	ListInt *t = listB;

	while (q != NULL) { 
		p = AppendInt(p, q->value);
		q = q->next;
	}
	while (t != NULL) { 
		p =AppendInt(p, t->value); 
		t = t->next;
	}

	return p; 
}


ListInt *CreateListInt(int *vect, int tam) {

	int i;
	ListInt *p = NULL;
	for (i = 0; i < tam; i++) {
		p = AppendInt(p, vect[i]);
	}
	return p;

}

ListInt *AppendInt(ListInt *list, int value) { 

	ListInt *p;

	if(list == NULL) { 
		list = malloc(sizeof(ListInt));
		list->value = value; 
		list->next = NULL;
	} else { 

		p = list;
		ListInt *new_node = malloc(sizeof(ListInt));
		new_node-> value = value; 
		new_node->next = NULL;

		while (p->next!= NULL) { 
			p = p->next; 
		}
		p->next = new_node;
	}

	return list;

}

ListInt *RemoveInt(ListInt *list, int value) { 


	if(list == NULL) { 
		return NULL;
	} 

	if(list->value == value) { 
		ListInt *p; 

		p = list->next; 
		free(list);

		return p; 
	}

	list->next = RemoveInt(list->next, value);

	return list;

}