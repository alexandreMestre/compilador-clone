#include<stdio.h>
#include<stdlib.h>
#include"data_structs.h"


tupleFieldReal* CreateTupleReal(double *initial_values, int size){

	tupleFieldReal *tuple_real = NULL;
	int i;

	for (i = 0; i < size; i++){
		tuple_real = CreateNewFieldReal(tuple_real, initial_values[i]);
	}

	return tuple_real;
}

tupleFieldReal* getLastPositionReal(tupleFieldReal *tuple){
	
	tupleFieldReal *localizer;
	localizer = tuple;
	while(localizer->next != NULL){
		localizer = localizer->next;	
	}
	return localizer;
}

tupleFieldReal* CreateNewFieldReal(tupleFieldReal *tuple, double value){

	/* Criando e configurando o novo campo a ser adicionado */
	tupleFieldReal *new_field, *last_field;
	new_field = malloc(sizeof(tupleFieldReal));
	new_field->value = value;

	/* Verificando se é o primeiro campo */
	if (tuple){
		last_field = getLastPositionReal(tuple);
		last_field->next = new_field;	
	} else {
		tuple = new_field;
		
	}
	return tuple;
}

void PrintTupleReal(tupleFieldReal* tuple){
	
	tupleFieldReal *localizer;
	localizer = tuple;
	int i = 0;
	
	printf("( ");
	while(localizer != NULL){

		printf("%lf ", localizer->value);
		localizer = localizer->next;
		i++;	
	}
    printf(")\n");
}

int getTupleIndexReal(tupleFieldReal *tuple, double value){

	tupleFieldReal *localizer;
	localizer = tuple;
	int index = 0;
	
	while (localizer && localizer->value != value){
		localizer = localizer->next;
		index += 1;
	}
	
	return index;
}
double getValueTupleReal(tupleFieldReal *tuple, int index){
	
	tupleFieldReal *localizer;
	localizer = tuple;
	int value;
	int counter = 0;
	
	
	while (localizer && counter != index){
		localizer = localizer->next;
		counter += 1;
	}

	return localizer->value;
}

tupleFieldReal* getTupleRealSlice(tupleFieldReal *tuple, int start_index, int end_index){


	tupleFieldReal *slice;
	int i;
	int slice_size = end_index - start_index;
	double *sliced_values = malloc(slice_size*sizeof(double));

	for(i = start_index; i < end_index; i++){
		sliced_values[i-start_index] = getValueTupleReal(tuple, i);
	}
	
	slice = CreateTupleReal(sliced_values, slice_size);
	PrintTupleReal(slice);
	return slice;
	
	
}

int SizeTupleReal(tupleFieldReal *tuple){

	tupleFieldReal *localizer;
	localizer = tuple;
	int size = 0;

	while(localizer){
		localizer = localizer->next;
		size += 1;	
	}

	return size;
}
tupleFieldReal* ConcatenateTupleReal(tupleFieldReal *tuple01, tupleFieldReal *tuple02){

	tupleFieldReal *concatenated_tuple;
	double *concatenated_tuple_values;

	int tuple01_size, tuple02_size, concatenated_tuple_size;
	int i;
	
	tuple01_size = SizeTupleReal(tuple01);
	tuple02_size = SizeTupleReal(tuple02);
	concatenated_tuple_size = tuple01_size + tuple02_size;

	concatenated_tuple_values = malloc(concatenated_tuple_size*sizeof(double));
	
	for(i = 0; i < tuple01_size; i++){
		concatenated_tuple_values[i] = getValueTupleReal(tuple01, i);
	}

	for(i = tuple01_size; i < concatenated_tuple_size; i++){
		concatenated_tuple_values[i] = getValueTupleReal(tuple02, i-tuple01_size);
	}
	
	concatenated_tuple = CreateTupleReal(concatenated_tuple_values, concatenated_tuple_size);
	PrintTupleReal(concatenated_tuple);
	return concatenated_tuple;

}

