#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "data_structs.h"

int lenListString(ListString *list) { 

	ListString *p = list; 
	int tam = 0; 
	while (p != NULL) { 
		p = p->next;
		tam++;
	}

	return tam;
}

int LowerEqualListString(ListString *listA, ListString *listB) { 

	int tamA = lenListString(listA);
	int tamB = lenListString(listB);

	if(tamA <= tamB) { 
		return 1; 
	} else { 
		return 0;
	}

	return 0;

}

int GreaterEqualListString(ListString *listA, ListString *listB) { 

	int tamA = lenListString(listA);
	int tamB = lenListString(listB);

	if(tamA >= tamB) { 
		return 1; 
	} else { 
		return 0;
	}

	return 0;

}

int EqualListString(ListString *listA, ListString *listB) { 

	int tamA = lenListString(listA);
	int tamB = lenListString(listB);

	if(tamA == tamB) { 
		ListString *p = listA;
		ListString *q = listB;

		while(q != NULL && p != NULL) { 
			if(strcmp(q->value,p->value) == 0) { 
				q = q->next;
				p = p->next;
			}
			else { 
				return 0;
			}

		}
	}
	else { 
		return 0; 
	}
	return 1; 
}

ListString *ConcatenateListString(ListString *listA, ListString *listB) { 

	ListString *p = NULL;
	ListString *q = listA; 
	ListString *t = listB;

	while (q != NULL) { 
		p = AppendString(p, q->value);
		q = q->next;
	}
	while (t != NULL) { 
		p =AppendString(p, t->value); 
		t = t->next;
	}

	return p; 
}


char **getArrayfromListString(ListString *list) { 

	int tam = lenListString(list); 
	char **array; 

	array = malloc(tam*sizeof(char)); 
	int i = 0; 
	ListString *p = list; 

	while(p != NULL) { 
		int tam_string = strlen(p->value);
		array[i] = malloc(tam_string*sizeof(char));
		strcpy(array[i], p->value); 
		p = p->next; 
		i++; 
	}

	return array; 
}

ListString *CreateListString(char **vect, int tam) {
	
	int i; 
	ListString *p = NULL;
	for (i = 0; i < tam; i++) {
		p = AppendString(p, vect[i]);
	}

	return p; 

}

ListString *AppendString(ListString *list, char *value) { 

	ListString *p;

	if(list == NULL) {  
		int word_size = strlen(value);
		list = malloc(sizeof(ListString));
		list->value = malloc(word_size*sizeof(char));
		strcpy(list->value,value); 
		list->next = NULL;

	} else { 

		p = list;
		ListString *new_node = malloc(sizeof(ListString));
		int word_size = strlen(value);
		new_node->value = malloc(word_size*sizeof(char));
		strcpy(new_node->value,value); 
		new_node->next = NULL;

		while (p->next!= NULL) { 
			p = p->next; 
		}
		p->next = new_node;
	}

	return list;

}

ListString *RemoveString(ListString *list, char *value) { 


	if(list == NULL) { 
		return NULL;
	} 

	if(strcmp(list->value,value) == 0) { 
		ListString *p; 

		p = list->next; 
		free(list);

		return p; 
	}

	list->next = RemoveString(list->next, value);

	return list;
}