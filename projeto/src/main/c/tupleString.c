#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"data_structs.h"


tupleFieldString* CreateTupleString(char **initial_values, int size){

	tupleFieldString *tuple_string = NULL;
	int i;
	
	
	for (i = 0; i < size; i++){
		tuple_string = CreateNewFieldString(tuple_string, initial_values[i]);
	}

	return tuple_string;
}

tupleFieldString* getLastPositionString(tupleFieldString *tuple){
	
	tupleFieldString *localizer;
	localizer = tuple;
	while(localizer->next != NULL){
		localizer = localizer->next;	
	}
	return localizer;
}

tupleFieldString* CreateNewFieldString(tupleFieldString *tuple, char *value){

	/* Criando e configurando o novo campo a ser adicionado */
	int word_size;
	tupleFieldString *new_field, *last_field;

	new_field = malloc(sizeof(tupleFieldString));
	word_size = strlen(value);
	new_field->value = malloc(word_size*sizeof(char));
	strcpy(new_field->value, value);
	
	/* Verificando se é o primeiro campo */
	if (tuple){
		last_field = getLastPositionString(tuple);
		last_field->next = new_field;	
	} else {
		tuple = new_field;
		
	}
	return tuple;
}

void PrintTupleString(tupleFieldString* tuple){
		
	tupleFieldString *localizer;
	localizer = tuple;
	int i = 0;

	printf("( ");
	while(localizer != NULL){

		printf("%s ", localizer->value);
		localizer = localizer->next;
		i++;	
	}
    printf(")\n");
}

int getTupleIndexString(tupleFieldString *tuple, char *value){

	tupleFieldString *localizer;
	localizer = tuple;
	int index = 0;
	
	while (localizer && strcmp(localizer->value, value) != 0){
		localizer = localizer->next;
		index += 1;
	}
	
	return index;
}
char* getValueTupleString(tupleFieldString *tuple, int index){
	
	tupleFieldString *localizer;
	localizer = tuple;
	int value;
	int counter = 0;
	
	
	while (localizer && counter != index){
		localizer = localizer->next;
		counter += 1;
	}

	return localizer->value;
}

tupleFieldString* getTupleStringSlice(tupleFieldString *tuple, int start_index, int end_index){


	tupleFieldString *slice;
	int i;
	int slice_size = end_index - start_index;
	char **sliced_values = malloc(slice_size*sizeof(char*));

	for(i = start_index; i < end_index; i++){
		sliced_values[i-start_index] = getValueTupleString(tuple, i);
	}
	
	slice = CreateTupleString(sliced_values, slice_size);
	PrintTupleString(slice);
	return slice;
}

int SizeTupleString(tupleFieldString *tuple){

	tupleFieldString *localizer;
	localizer = tuple;
	int size = 0;

	while(localizer){
		localizer = localizer->next;
		size += 1;	
	}

	return size;
}

tupleFieldString* ConcatenateTupleString(tupleFieldString *tuple01, tupleFieldString *tuple02){

	tupleFieldString *concatenated_tuple;
	char **concatenated_tuple_values;

	int tuple01_size, tuple02_size, concatenated_tuple_size;
	int i;
	
	tuple01_size = SizeTupleString(tuple01);
	tuple02_size = SizeTupleString(tuple02);
	concatenated_tuple_size = tuple01_size + tuple02_size;

	concatenated_tuple_values = malloc(concatenated_tuple_size*sizeof(char*));
	
	for(i = 0; i < tuple01_size; i++){
		concatenated_tuple_values[i] = getValueTupleString(tuple01, i);
	}

	for(i = tuple01_size; i < concatenated_tuple_size; i++){
		concatenated_tuple_values[i] = getValueTupleString(tuple02, i-tuple01_size);
	}
	
	concatenated_tuple = CreateTupleString(concatenated_tuple_values, concatenated_tuple_size);
	PrintTupleString(concatenated_tuple);
	return concatenated_tuple;

}



