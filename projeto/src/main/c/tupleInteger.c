#include<stdio.h>
#include<stdlib.h>
#include"data_structs.h"


tupleFieldInteger* CreateTupleInteger(int *initial_values, int size){

	tupleFieldInteger *tuple_int = NULL;
	int i;

	for (i = 0; i < size; i++){
		tuple_int = CreateNewFieldInteger(tuple_int, initial_values[i]);
	}

	return tuple_int;
}

tupleFieldInteger* getLastPositionInteger(tupleFieldInteger *tuple){
	
	tupleFieldInteger *localizer;
	localizer = tuple;
	while(localizer->next != NULL){
		localizer = localizer->next;	
	}
	return localizer;
}

tupleFieldInteger* CreateNewFieldInteger(tupleFieldInteger *tuple, int value){

	/* Criando e configurando o novo campo a ser adicionado */
	tupleFieldInteger *new_field, *last_field;
	new_field = malloc(sizeof(tupleFieldInteger));
	new_field->value = value;

	/* Verificando se é o primeiro campo */
	if (tuple){
		last_field = getLastPositionInteger(tuple);
		last_field->next = new_field;	
	} else {
		tuple = new_field;
		
	}
	return tuple;
}

void PrintTupleInteger(tupleFieldInteger* tuple){
	
	tupleFieldInteger *localizer;
	localizer = tuple;
	int i = 0;
	printf("( ");
	while(localizer != NULL){

		//printf("Position: %d\n", i);
		//printf("Value: %d\n", localizer->value);
		printf("%d ", localizer->value);
		localizer = localizer->next;
		i++;	
	}
	printf(")\n");
}

int getTupleIndexInteger(tupleFieldInteger *tuple, int value){

	tupleFieldInteger *localizer;
	localizer = tuple;
	int index = 0;
	
	while (localizer && localizer->value != value){
		localizer = localizer->next;
		index += 1;
	}
	
	return index;
}
int getValueTupleInteger(tupleFieldInteger *tuple, int index){
	
	tupleFieldInteger *localizer;
	localizer = tuple;
	int value;
	int counter = 0;
	
	
	while (localizer && counter != index){
		localizer = localizer->next;
		counter += 1;
	}

	return localizer->value;
}

tupleFieldInteger* getTupleIntegerSlice(tupleFieldInteger *tuple, int start_index, int end_index){


	tupleFieldInteger *slice;
	int i;
	int slice_size = end_index - start_index;
	int *sliced_values = malloc(slice_size*sizeof(int));

	for(i = start_index; i < end_index; i++){
		sliced_values[i-start_index] = getValueTupleInteger(tuple, i);
	}
	
	slice = CreateTupleInteger(sliced_values, slice_size);
	PrintTupleInteger(slice);
	return slice;

}

int SizeTupleInteger(tupleFieldInteger *tuple){

	tupleFieldInteger *localizer;
	localizer = tuple;
	int size = 0;

	while(localizer){
		localizer = localizer->next;
		size += 1;	
	}

	return size;
}
tupleFieldInteger* ConcatenateTupleInteger(tupleFieldInteger *tuple01, tupleFieldInteger *tuple02){

	tupleFieldInteger *concatenated_tuple;
	int *concatenated_tuple_values;

	int tuple01_size, tuple02_size, concatenated_tuple_size;
	int i;
	
	tuple01_size = SizeTupleInteger(tuple01);
	tuple02_size = SizeTupleInteger(tuple02);
	concatenated_tuple_size = tuple01_size + tuple02_size;

	concatenated_tuple_values = malloc(concatenated_tuple_size*sizeof(int));
	
	for(i = 0; i < tuple01_size; i++){
		concatenated_tuple_values[i] = getValueTupleInteger(tuple01, i);
	}

	for(i = tuple01_size; i < concatenated_tuple_size; i++){
		concatenated_tuple_values[i] = getValueTupleInteger(tuple02, i-tuple01_size);
	}
	
	concatenated_tuple = CreateTupleInteger(concatenated_tuple_values, concatenated_tuple_size);
	PrintTupleInteger(concatenated_tuple);
	return concatenated_tuple;

}


