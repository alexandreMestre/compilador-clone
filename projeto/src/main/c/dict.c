/* @author Gabriel Capiteli Bertocco RA 155419*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"data_structs.h"

#define MAX_SIZE 100

dictField* getLastPosition(dictField *dict){
	
	dictField *localizer;
	localizer = dict;
	while(localizer->next != NULL){
		localizer = localizer->next;	
	}
	return localizer;
}

dictField* CreateNewField(dictField *dict, char *key_name, char *string, int *integer, double *real_number, int *boolean){

	/* Criando e configurando o novo campo a ser adicionado */
	dictField *new_field, *last_field;
	new_field = malloc(sizeof(dictField));
	new_field->key = malloc(MAX_SIZE*sizeof(char));
	strcpy(new_field->key, key_name);
	
	if (string){
		new_field->string = string;
		new_field->integer = NULL;
		new_field->real_number = NULL;
		new_field->boolean = NULL;
		new_field->next = NULL;	

	} else if (integer){
		new_field->string = NULL;
		new_field->integer = integer;
		new_field->real_number = NULL;
		new_field->boolean = NULL;
		new_field->next = NULL;	

	} else if (real_number) {
		new_field->string = NULL;
		new_field->integer = NULL;
		new_field->real_number = real_number;
		new_field->boolean = NULL;
		new_field->next = NULL;	

	} else if (boolean){
		new_field->string = NULL;
		new_field->integer = NULL;
		new_field->real_number = NULL;
		new_field->boolean = boolean;
		new_field->next = NULL;	
	}

	/* Verificando se é o primeiro campo */
	
	if (dict){
		last_field = getLastPosition(dict);
		last_field->next = new_field;	
	} else {
		dict = new_field;
		
	}
	return dict;
}

void PrintFieldsDict(dictField *dict){
	
	dictField *localizer;
	localizer = dict;
	int i = 1;
	while(localizer != NULL){

		printf("Field %d\n", i);
		printf("Key name: %s\n", localizer->key);

		if (localizer->string){
			printf("string: %s\n", localizer->string);
		} else if (localizer->integer){
			printf("integer: %d\n", *localizer->integer);
		} else if (localizer->real_number){
			printf("Real number: %lf\n", *localizer->real_number);
		} else if (localizer->boolean){
			if (*localizer->boolean == 1)
				printf("boolean: true\n");
			else
				printf("boolean: false\n");
		}
		printf("\n");
		localizer = localizer->next;
		i++;	
	}
}

int getIndexOfField(dictField *dict, char *key){

	dictField *localizer;
	localizer = dict;
	int index, a;
	

	while (localizer && (strcmp(localizer->key, key) != 0)){
		localizer = localizer->next;
	}
	if (localizer) {
		if (localizer->string){
			index = 1;
		}
		else if (localizer->integer){
			index = 2;
		}
		else if (localizer->real_number){
			index = 3;
		}		
		else if (localizer->boolean){
			index = 4;
		}
	}
	return index;
}
void* getValue(dictField *dict, char *key){
	
	dictField *localizer;
	localizer = dict;
	void *value;
	
	while (localizer && (strcmp(localizer->key, key) != 0)){
		localizer = localizer->next;
	}
	if (localizer) {
		if (localizer->string){
			value = (void *)localizer->string;
		}
		else if (localizer->integer){
			value = (void *)localizer->integer;
		}
		else if (localizer->real_number){
			value = (void *)localizer->real_number;
		}		
		else if (localizer->boolean){
			value = (void *)localizer->boolean;
		}
	}
	return value;
}

char* getKeyOfField(dictField *dict, char *string, int *integer, double *real_number, int *boolean){
	
	char *key_value;
	dictField *localizer;
	localizer = dict;

	key_value = malloc(MAX_SIZE*sizeof(char));
	
	if (string){
		while (localizer && (!localizer->string || (strcmp(localizer->string, string) != 0))){
			localizer = localizer->next;
		}
		strcpy(key_value, localizer->key);

	} else if (integer){
		while (localizer && (!localizer->integer || (*localizer->integer != *integer))){
			localizer = localizer->next;
		}
		strcpy(key_value, localizer->key);

	} else if (real_number){

		while (localizer && (!localizer->real_number || (*localizer->real_number != *real_number))){
			localizer = localizer->next;
		}
		strcpy(key_value, localizer->key);
	
	} else if (boolean){
		while (localizer && (!localizer->boolean || (*localizer->boolean != *boolean))){
			localizer = localizer->next;
		}
		strcpy(key_value, localizer->key);
	}
	
	return key_value;
}

dictField* RemoveFieldByKey(dictField *dict, char *key_name){

	dictField *localizer;
	localizer = dict;
	
	if (strcmp(localizer->key, key_name) == 0){
		dict = localizer->next;
		free(localizer);
	} else {
		while (localizer->next && (strcmp(localizer->next->key, key_name) != 0)){
			localizer = localizer->next;
		}
		free(localizer->next);
		localizer->next = localizer->next->next;
		
	}

	return dict;
}
