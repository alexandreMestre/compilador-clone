#define MAX_SIZE 100
#define MAX_STRING_TAM 21

/* struct de suporte a dicionários */
typedef struct dictField{
	char *key;
	char *string;
	int	 *integer;
	double *real_number;
	int *boolean;
	struct dictField *next;
} dictField;

typedef struct ListInt { 
	int value; 
	struct ListInt *next; 
} ListInt;

typedef struct ListDouble { 
	double value; 
	struct ListDouble *next; 
} ListDouble;

typedef struct ListString { 
	char *value; 
	struct ListString *next; 
} ListString;


/* structs the suporte a tuplas */
typedef struct tupleFieldInteger {
	int value;
	struct tupleFieldInteger *next;
} tupleFieldInteger;

typedef struct tupleFieldString {
	char* value;
	struct tupleFieldString *next;
} tupleFieldString;

typedef struct tupleFieldReal {
	double value;
	struct tupleFieldReal *next;
} tupleFieldReal;

/* Métodos para operações em tuplas de Int's e Boolean's */
tupleFieldInteger* CreateTupleInteger(int *initial_values, int size);
tupleFieldInteger* getLastPositionInteger(tupleFieldInteger *tuple);
tupleFieldInteger* CreateNewFieldInteger(tupleFieldInteger *tuple, int value);
void PrintTupleInteger(tupleFieldInteger* tuple);
int getTupleIndexInteger(tupleFieldInteger *tuple, int value);
int getValueTupleInteger(tupleFieldInteger *tuple, int index);
tupleFieldInteger* getTupleIntegerSlice(tupleFieldInteger *tuple, int start_index, int end_index);
tupleFieldInteger* ConcatenateTupleInteger(tupleFieldInteger *tuple01, tupleFieldInteger *tuple02);
int SizeTupleInteger(tupleFieldInteger *tuple);

/* Métodos para operações em tuplas de Double's */
tupleFieldReal* CreateTupleReal(double *initial_values, int size);
tupleFieldReal* getLastPositionReal(tupleFieldReal *tuple);
tupleFieldReal* CreateNewFieldReal(tupleFieldReal *tuple, double value);
void PrintTupleReal(tupleFieldReal* tuple);
int getTupleIndexReal(tupleFieldReal *tuple, double value);
double getValueTupleReal(tupleFieldReal *tuple, int index);
tupleFieldReal* getTupleRealSlice(tupleFieldReal *tuple, int start_index, int end_index);
int SizeTupleReal(tupleFieldReal *tuple);
tupleFieldReal* ConcatenateTupleReal(tupleFieldReal *tuple01, tupleFieldReal *tuple02);

/* Métodos para operações em tuplas de String's */
tupleFieldString* CreateTupleString(char **initial_values, int size);
tupleFieldString* getLastPositionString(tupleFieldString *tuple);
tupleFieldString* CreateNewFieldString(tupleFieldString *tuple, char *value);
void PrintTupleString(tupleFieldString* tuple);
int getTupleIndexString(tupleFieldString *tuple, char *value);
char* getValueTupleString(tupleFieldString *tuple, int index);
tupleFieldString* getTupleStringSlice(tupleFieldString *tuple, int start_index, int end_index);
int SizeTupleString(tupleFieldString *tuple);
tupleFieldString* ConcatenateTupleString(tupleFieldString *tuple01, tupleFieldString *tuple02);


// Metodos para operacoes em dicionários
dictField* getLastPosition(dictField *dict);
dictField* CreateNewField(dictField *dict, char *key_name, char *string, int *integer, double *real_number, int *boolean);
void PrintFieldsDict(dictField *dict);
void* getValue(dictField *dict, char *key);
int getIndexOfField(dictField *dict, char *key);
char* getKeyOfField(dictField *dict, char *string, int *integer, double *real_number, int *boolean);
dictField* RemoveFieldByKey(dictField *dict, char *key_name);

//Metodos para operacoes em listas de Inteiros
ListInt *AppendInt(ListInt *list, int value);
ListInt *RemoveInt(ListInt *list, int value);
int MinListInt(ListInt *list);
int MaxListInt(ListInt *list);
int lenListString(ListString *list);
int *getArrayfromListInt(ListInt *list);
int LowerEqualListInt(ListInt *listA, ListInt *listB);
int GreaterEqualListInt(ListInt *listA, ListInt *listB);
int EqualListInt(ListInt *listA, ListInt *listB);
ListInt *ConcatenateListInt(ListInt *listA, ListInt *listB);
ListInt *CreateListInt(int *vect, int tam);

//Metodos para operacoes em listas de Double
double MaxListDouble(ListDouble *list);
double MinListDouble(ListDouble *list);
ListDouble *AppendDouble(ListDouble *list, double value);
ListDouble *RemoveDouble(ListDouble *list, double value);
int lenListDouble(ListDouble *list);
double *getArrayfromListDouble(ListDouble *list);
int LowerEqualListDouble(ListDouble *listA, ListDouble *listB);
int GreaterEqualListDouble(ListDouble *listA, ListDouble *listB);
int EqualListDouble(ListDouble *listA, ListDouble *listB);
ListDouble *ConcatenateListDouble(ListDouble *listA, ListDouble *listB);
ListDouble *CreateListDouble(double *vect, int tam);


//Metodos para operacoes de Listas de Strings em C
ListString *RemoveString(ListString *list, char *value);
ListString *AppendString(ListString *list, char *value);
int lenListString(ListString *list);
char **getArrayfromListString(ListString *list);
int LowerEqualListString(ListString *listA, ListString *listB);
int GreaterEqualListString(ListString *listA, ListString *listB);
int EqualListString(ListString *listA, ListString *listB);
ListString *ConcatenateListString(ListString *listA, ListString *listB);
ListString *CreateListString(char **vect, int tam);

//Metodos para strings
int lenString(char *string);
char *ConcatenateStrings(char *string1, char *string2);
int LowerEqualStrings(char *string1, char *string2);
int GreaterEqualStrings(char *string1, char *string2);
EqualStrings(char *string1, char *string2);

