#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "data_structs.h"


int lenListDouble(ListDouble *list) { 

	ListDouble *p = list; 
	int tam = 0; 
	while (p != NULL) { 
		p = p->next;
		tam++;
	}

	return tam;
}

int LowerEqualListDouble(ListDouble *listA, ListDouble *listB) { 

	int tamA = lenListDouble(listA);
	int tamB = lenListDouble(listB);

	if(tamA <= tamB) { 
		return 1; 
	} else { 
		return 0;
	}

	return 0;

}

int GreaterEqualListDouble(ListDouble *listA, ListDouble *listB) { 

	int tamA = lenListDouble(listA);
	int tamB = lenListDouble(listB);

	if(tamA >= tamB) { 
		return 1; 
	} else { 
		return 0;
	}

	return 0;

}

int EqualListDouble(ListDouble *listA, ListDouble *listB) { 

	int tamA = lenListDouble(listA);
	int tamB = lenListDouble(listB);

	if(tamA == tamB) { 
		ListDouble *p = listA;
		ListDouble *q = listB;

		while(q != NULL && p != NULL) { 
			if(q->value == p->value) { 
				q = q->next;
				p = p->next;
			}
			else { 
				return 0;
			}

		}
	}
	else { 
		return 0; 
	}
	return 1; 
}

double *getArrayfromListDouble(ListDouble *list) { 

	int tam = lenListDouble(list); 
	double *array; 

	array = malloc(tam*sizeof(double)); 
	int i = 0; 
	ListDouble *p = list; 

	while(p != NULL) { 
		array[i] = p->value; 
		p = p->next; 
		i++; 
	}

	return array; 
}

double MaxListDouble(ListDouble *list) { 
	double max = list->value;
	ListDouble *p = list; 
	while(p != NULL) { 
		if(p->value > max) { 
			max = p->value;
		}	
		p = p->next; 
	}

	return max; 
}

double MinListDouble(ListDouble *list) { 
	double min = list->value; 
	ListDouble *p = list; 
	while (p != NULL) { 
		if(p->value < min) { 
			min = p->value; 
		}
		p = p->next;
	}
	return min;
}


ListDouble *CreateListDouble(double *vect, int tam) {
	
	int i; 
	ListDouble *p = NULL;
	for (i = 0; i < tam; i++) { 
		p = AppendDouble(p, vect[i]);
	}
	return p; 

}

ListDouble *ConcatenateListDouble(ListDouble *listA, ListDouble *listB) { 

	ListDouble *p = NULL;
	ListDouble *q = listA; 
	ListDouble *t = listB;

	while (q != NULL) { 
		p = AppendDouble(p, q->value);
		q = q->next;
	}
	while (t != NULL) { 
		p =AppendDouble(p, t->value); 
		t = t->next;
	}

	return p; 
}



ListDouble *AppendDouble(ListDouble *list, double value) { 

	ListDouble *p;

	if(list == NULL) { 
		list = malloc(sizeof(ListDouble));
		list->value = value; 
		list->next = NULL;
	} else { 

		p = list;
		ListDouble *new_node = malloc(sizeof(ListDouble));
		new_node-> value = value; 
		new_node->next = NULL;

		while (p->next!= NULL) { 
			p = p->next; 
		}
		p->next = new_node;
	}

	return list;

}

ListDouble *RemoveDouble(ListDouble *list, double value) { 


	if(list == NULL) { 
		return NULL;
	} 

	if(list->value == value) { 
		ListDouble *p; 

		p = list->next; 
		free(list);

		return p; 
	}

	list->next = RemoveDouble(list->next, value);

	return list;

}