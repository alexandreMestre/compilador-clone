package Application;

import javax.swing.*;

public class Application {
    public static void main(String args []) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                FileSelectionView selectionView = new FileSelectionView();
                selectionView.setVisible(true);
            }
        });
    }
}
