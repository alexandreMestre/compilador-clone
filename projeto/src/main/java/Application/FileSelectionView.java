package Application;

import conversor.ConcreteConversor;
import conversor.ConcreteConversorHelper;
import conversor.Conversor;
import conversor.ConversorHelper;
import errorHandler.ConcreteErrorHandler;
import errorHandler.ErrorHandler;
import parser.Compiler;
import semanticAnalyzer.ConcreteSemanticAnalyzer;
import semanticAnalyzer.ConcreteSymbolTable;
import semanticAnalyzer.SemanticAnalyzer;

import java.util.ArrayList;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import helper.*;

public class FileSelectionView extends JFrame {
    private JPanel rootPanel;
    private JTextField originPathField;
    private JTextField destinationPathField;
    private JButton compileButton;
    private JTextArea compilationLogTextArea;

    public FileSelectionView() {
        add(rootPanel);

        setTitle("Compilador");
        setSize(700, 400);
        compilationLogTextArea.setText("");

        compileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                FileInputStream input = null;
                compilationLogTextArea.setText("");
                try {
                    String originPath = originPathField.getText();
                    File pythonFile = new File(originPath);
                    input = new FileInputStream(pythonFile);

                    String destinationPath = destinationPathField.getText() + "/"
                            + pythonFile.getName().replace(".py", ".c");

                    // cria um novo indentationHelper, conversorHelper, errorHandler, semanticAnalyzer a cada ação pra não manter o estado de compilação do arquivo anterior
                    ConversorHelper conversorHelper = new ConcreteConversorHelper();
                    IndentationHelper indentationHelper = new ConcreteIndentationHelper();
                    ErrorHandler errorHandler = new ConcreteErrorHandler();
                    SemanticAnalyzer semanticAnalyzer = new ConcreteSemanticAnalyzer(new ConcreteSymbolTable());

                    Conversor conversor = new ConcreteConversor(conversorHelper,
                                                                destinationPath);
                                                                
                    Compiler compiler = new Compiler(input, semanticAnalyzer,
                                                     conversor, errorHandler,
                                                     indentationHelper);

                    ArrayList<Exception> exceptions = compiler.execute();

                    if(exceptions != null && exceptions.isEmpty() == false) {
                        String fullMessage = "";
                        for(Exception exception : exceptions) {
                            String formattedMessage = FormatterHelper.add_escapes(exception.getMessage());
                            fullMessage += formattedMessage + "\n";
                        }
                        fullMessage += "Ocorreram erros de compilação e o arquivo C não será gerado.";
                        compilationLogTextArea.setText(fullMessage);

                    } else {
                        JOptionPane.showMessageDialog(null, "Arquivo gerado com sucesso");
                        compilationLogTextArea.setText("Compilação realizada com sucesso.");
                    }

                } catch (Exception exception) {
                    JOptionPane.showMessageDialog(null,
                                                  "Erro: " + exception.getLocalizedMessage());
                    System.out.println(exception.getLocalizedMessage());
                }
            }
        });

        originPathField.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String title = "Selecione o arquivo python";
                String selectedFilePath = selectFile(pythonFileFilter(), title);
                if (!selectedFilePath.isEmpty()) {
                    originPathField.setText(selectedFilePath);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) { }
            @Override
            public void mouseReleased(MouseEvent e) { }
            @Override
            public void mouseEntered(MouseEvent e) { }
            @Override
            public void mouseExited(MouseEvent e) { }
        });

        destinationPathField.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String title = "Selecione o diretório para salvar o arquivo C";
                String selectedFilePath = selectFile(folderFilter(), title);
                if (!selectedFilePath.isEmpty()) {
                    destinationPathField.setText(selectedFilePath);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) { }
            @Override
            public void mouseReleased(MouseEvent e) { }
            @Override
            public void mouseEntered(MouseEvent e) { }
            @Override
            public void mouseExited(MouseEvent e) { }
        });
    }

    public String selectFile(FileFilter filter, String title) {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle(title);
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int returnVal = fc.showOpenDialog(FileSelectionView.this);

        if (returnVal == JFileChooser.APPROVE_OPTION && filter.accept(fc.getSelectedFile())) {
            File selectedFile = fc.getSelectedFile();
            return selectedFile.getAbsolutePath();
        } else {
            return "";
        }
    }

    public FileFilter pythonFileFilter() {
        return new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.getAbsolutePath().endsWith(".py");
            }

            @Override
            public String getDescription() {
                return "Código python";
            }
        };
    }

    public FileFilter folderFilter() {
        return new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "Pasta";
            }
        };
    }
}
