package Application;

import conversor.ConcreteConversor;
import conversor.ConcreteConversorHelper;
import conversor.Conversor;
import conversor.ConversorHelper;
import errorHandler.ConcretePrinterErrorHandler;
import errorHandler.ErrorHandler;
import helper.ConcreteIndentationHelper;
import helper.IndentationHelper;
import parser.Compiler;
import semanticAnalyzer.ConcreteSemanticAnalyzer;
import semanticAnalyzer.ConcreteSymbolTable;
import semanticAnalyzer.SemanticAnalyzer;

public class TerminalApplication {
    public static void main(String args []) {

        SemanticAnalyzer semanticAnalyzer = new ConcreteSemanticAnalyzer(new ConcreteSymbolTable());
        ConversorHelper conversorHelper = new ConcreteConversorHelper();
        ErrorHandler errorHandler = new ConcretePrinterErrorHandler();
        IndentationHelper indentationHelper = new ConcreteIndentationHelper();

        Conversor conversor = new ConcreteConversor(conversorHelper, null);

        Compiler compiler = new Compiler(System.in, semanticAnalyzer,
                conversor, errorHandler, indentationHelper);

        compiler.execute();
    }
}
