package semanticAnalyzer

import model.ExpressionType
import model.OperationType

typealias SymbolMethod = ((OperationType) -> ExpressionType?)

class OperationsTable {

    companion object {

        @Throws(Exception::class)
        fun expressionType(firstExpressionType: ExpressionType, secondExpressionType: ExpressionType, operationType: OperationType): ExpressionType {
            if(firstExpressionType == ExpressionType.ERROR() || secondExpressionType == ExpressionType.ERROR()) {
                return ExpressionType.ERROR()
            }
            if (operationType == OperationType.NOT_EQUAL || operationType == OperationType.EQUAL) {
                return ExpressionType.BOOL()
            }
            val expressionType = when (firstExpressionType) {
                is ExpressionType.INT -> expressionTypeForInt(secondExpressionType, operationType)
                is ExpressionType.DOUBLE -> expressionTypeForDouble(secondExpressionType, operationType)
                is ExpressionType.STRING -> expressionTypeForString(secondExpressionType, operationType)
                is ExpressionType.BOOL -> expressionTypeForBool(secondExpressionType, operationType)
                is ExpressionType.ARRAY -> expressionTypeForArray(firstExpressionType.expressionType, secondExpressionType, operationType)
                is ExpressionType.TUPLE -> expressionTypeForTuple(firstExpressionType.expressionType, secondExpressionType, operationType)
                is ExpressionType.DICTIONARY -> null
                is ExpressionType.EMPTY -> null
                is ExpressionType.ERROR -> null
            }
            return expressionType ?: throw SemanticAnalyzerException.Operation(operationType, firstExpressionType, secondExpressionType)
        }

        @Throws(Exception::class)
        fun expressionType(firstExpressionType: ExpressionType, operationType: OperationType): ExpressionType {
            if (firstExpressionType is ExpressionType.BOOL && operationType == OperationType.NOT) {
                return ExpressionType.BOOL()
            } else {
                throw SemanticAnalyzerException.Operation(operationType, firstExpressionType)
            }
        }

        //

        private fun expressionTypeForInt(secondExpressionType: ExpressionType, operationType: OperationType): ExpressionType? {
            return when (secondExpressionType) {
                is ExpressionType.INT -> expressionTypeForIntInt(operationType)
                is ExpressionType.DOUBLE -> expressionTypeForIntDouble(operationType)
                is ExpressionType.STRING -> expressionTypeForIntString(operationType)
                is ExpressionType.BOOL -> expressionTypeForIntBool(operationType)
                is ExpressionType.ARRAY -> expressionTypeForIntArray(secondExpressionType.expressionType, operationType)
                is ExpressionType.TUPLE -> expressionTypeForIntTuple(secondExpressionType.expressionType, operationType)
                is ExpressionType.DICTIONARY -> null
                is ExpressionType.EMPTY -> null
                is ExpressionType.ERROR -> null
            }
        }

        private fun expressionTypeForDouble(secondExpressionType: ExpressionType, operationType: OperationType): ExpressionType? {
            return when (secondExpressionType) {
                is ExpressionType.INT -> expressionTypeForIntDouble(operationType)
                is ExpressionType.DOUBLE -> expressionTypeForDoubleDouble(operationType)
                is ExpressionType.STRING -> null
                is ExpressionType.BOOL -> expressionTypeForDoubleBool(operationType)
                is ExpressionType.ARRAY -> null
                is ExpressionType.TUPLE -> null
                is ExpressionType.DICTIONARY -> null
                is ExpressionType.EMPTY -> null
                is ExpressionType.ERROR -> null
            }
        }

        private fun expressionTypeForString(secondExpressionType: ExpressionType, operationType: OperationType): ExpressionType? {
            return when (secondExpressionType) {
                is ExpressionType.INT -> expressionTypeForIntString(operationType)
                is ExpressionType.DOUBLE -> null
                is ExpressionType.STRING -> expressionTypeForStringString(operationType)
                is ExpressionType.BOOL -> expressionTypeForStringBool(operationType)
                is ExpressionType.ARRAY -> expressionTypeForStringArray(operationType)
                is ExpressionType.TUPLE -> null
                is ExpressionType.DICTIONARY -> expressionTypeForStringDictionary(operationType)
                is ExpressionType.EMPTY -> null
                is ExpressionType.ERROR -> null
            }
        }

        private fun expressionTypeForBool(secondExpressionType: ExpressionType, operationType: OperationType): ExpressionType? {
            return when (secondExpressionType) {
                is ExpressionType.INT -> expressionTypeForIntBool(operationType)
                is ExpressionType.DOUBLE -> expressionTypeForDoubleBool(operationType)
                is ExpressionType.STRING -> expressionTypeForStringBool(operationType)
                is ExpressionType.BOOL -> expressionTypeForBoolBool(operationType)
                is ExpressionType.ARRAY -> expressionTypeForArrayBool(secondExpressionType.expressionType, operationType)
                is ExpressionType.TUPLE -> expressionTypeForTupleBool(secondExpressionType.expressionType, operationType)
                is ExpressionType.DICTIONARY -> null
                is ExpressionType.EMPTY -> null
                is ExpressionType.ERROR -> null
            }
        }

        private fun expressionTypeForArray(arrayExpressionType: ExpressionType, secondExpressionType: ExpressionType, operationType: OperationType): ExpressionType? {
            return when (secondExpressionType) {
                is ExpressionType.INT -> expressionTypeForIntArray(arrayExpressionType, operationType)
                is ExpressionType.DOUBLE -> null
                is ExpressionType.STRING -> null
                is ExpressionType.BOOL -> expressionTypeForArrayBool(arrayExpressionType, operationType)
                is ExpressionType.ARRAY -> expressionTypeForArrayArray(arrayExpressionType, secondExpressionType.expressionType, operationType)
                is ExpressionType.TUPLE -> null
                is ExpressionType.DICTIONARY -> null
                is ExpressionType.EMPTY -> null
                is ExpressionType.ERROR -> null
            }
        }

        private fun expressionTypeForTuple(tupleExpressionType: ExpressionType, secondExpressionType: ExpressionType, operationType: OperationType): ExpressionType? {
            return when (secondExpressionType) {
                is ExpressionType.INT -> expressionTypeForIntTuple(tupleExpressionType, operationType)
                is ExpressionType.DOUBLE -> null
                is ExpressionType.STRING -> null
                is ExpressionType.BOOL -> expressionTypeForTupleBool(tupleExpressionType, operationType)
                is ExpressionType.ARRAY -> null
                is ExpressionType.TUPLE -> expressionTypeForTupleTuple(tupleExpressionType, secondExpressionType.expressionType, operationType)
                is ExpressionType.DICTIONARY -> null
                is ExpressionType.EMPTY -> null
                is ExpressionType.ERROR -> null
            }
        }

        private fun expressionTypeForDictionary(secondExpressionType: ExpressionType, operationType: OperationType): ExpressionType? {
            return when (secondExpressionType) {
                is ExpressionType.INT -> null
                is ExpressionType.DOUBLE -> null
                is ExpressionType.STRING -> null
                is ExpressionType.BOOL -> null
                is ExpressionType.ARRAY -> null
                is ExpressionType.TUPLE -> null
                is ExpressionType.DICTIONARY -> null
                is ExpressionType.EMPTY -> null
                is ExpressionType.ERROR -> null
            }
        }

        //

        private fun expressionTypeForIntInt(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.PLUS -> ExpressionType.INT()
                OperationType.MINUS -> ExpressionType.INT()
                OperationType.MULTIPLICATION -> ExpressionType.INT()
                OperationType.DIVISION -> ExpressionType.INT()
                OperationType.MOD -> ExpressionType.INT()
                OperationType.GREATER -> ExpressionType.BOOL()
                OperationType.LOWER -> ExpressionType.BOOL()
                OperationType.GREATER_EQUAL -> ExpressionType.BOOL()
                OperationType.LOWER_EQUAL -> ExpressionType.BOOL()
                OperationType.POW -> ExpressionType.INT()
                else -> null
            }
        }

        private fun expressionTypeForDoubleDouble(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.PLUS -> ExpressionType.DOUBLE()
                OperationType.MINUS -> ExpressionType.DOUBLE()
                OperationType.MULTIPLICATION -> ExpressionType.DOUBLE()
                OperationType.DIVISION -> ExpressionType.DOUBLE()
                OperationType.MOD -> ExpressionType.DOUBLE()
                OperationType.GREATER -> ExpressionType.BOOL()
                OperationType.LOWER -> ExpressionType.BOOL()
                OperationType.GREATER_EQUAL -> ExpressionType.BOOL()
                OperationType.LOWER_EQUAL -> ExpressionType.BOOL()
                OperationType.POW -> ExpressionType.DOUBLE()
                else -> null
            }
        }

        private fun expressionTypeForArrayArray(firstArrayExpressionType: ExpressionType,
                                                secondArrayExpressionType: ExpressionType,
                                                operation: OperationType): ExpressionType? {
            if (firstArrayExpressionType != secondArrayExpressionType) {
                return null
            }
            return when (operation) {
                OperationType.PLUS -> ExpressionType.ARRAY(firstArrayExpressionType)
                OperationType.GREATER -> ExpressionType.BOOL()
                OperationType.LOWER -> ExpressionType.BOOL()
                OperationType.GREATER_EQUAL -> ExpressionType.BOOL()
                OperationType.LOWER_EQUAL -> ExpressionType.BOOL()
                else -> null
            }
        }

        private fun expressionTypeForTupleTuple(firstTupleExpressionType: ExpressionType,
                                                secondTupleExpressionType: ExpressionType,
                                                operation: OperationType): ExpressionType? {
            if (firstTupleExpressionType != secondTupleExpressionType) {
                return null
            }
            return when (operation) {
                OperationType.PLUS -> ExpressionType.TUPLE(firstTupleExpressionType)
                OperationType.GREATER -> ExpressionType.BOOL()
                OperationType.LOWER -> ExpressionType.BOOL()
                OperationType.GREATER_EQUAL -> ExpressionType.BOOL()
                OperationType.LOWER_EQUAL -> ExpressionType.BOOL()
                else -> null
            }
        }

        private fun expressionTypeForStringString(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.PLUS -> ExpressionType.STRING()
                OperationType.GREATER -> ExpressionType.BOOL()
                OperationType.LOWER -> ExpressionType.BOOL()
                OperationType.GREATER_EQUAL -> ExpressionType.BOOL()
                OperationType.LOWER_EQUAL -> ExpressionType.BOOL()
                else -> null
            }
        }


        private fun expressionTypeForBoolBool(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.PLUS -> ExpressionType.INT()
                OperationType.MINUS -> ExpressionType.INT()
                OperationType.MULTIPLICATION -> ExpressionType.INT()
                OperationType.DIVISION -> ExpressionType.DOUBLE()
                OperationType.MOD -> ExpressionType.INT()
                OperationType.GREATER -> ExpressionType.BOOL()
                OperationType.LOWER -> ExpressionType.BOOL()
                OperationType.GREATER_EQUAL -> ExpressionType.BOOL()
                OperationType.LOWER_EQUAL -> ExpressionType.BOOL()
                OperationType.AND -> ExpressionType.BOOL()
                OperationType.OR -> ExpressionType.BOOL()
                OperationType.POW -> ExpressionType.INT()
                else -> null
            }
        }

        private fun expressionTypeForIntDouble(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.PLUS -> ExpressionType.DOUBLE()
                OperationType.MINUS -> ExpressionType.DOUBLE()
                OperationType.MULTIPLICATION -> ExpressionType.DOUBLE()
                OperationType.DIVISION -> ExpressionType.DOUBLE()
                OperationType.MOD -> ExpressionType.DOUBLE()
                OperationType.GREATER -> ExpressionType.BOOL()
                OperationType.LOWER -> ExpressionType.BOOL()
                OperationType.GREATER_EQUAL -> ExpressionType.BOOL()
                OperationType.LOWER_EQUAL -> ExpressionType.BOOL()
                OperationType.POW -> ExpressionType.DOUBLE()
                else -> null
            }
        }

        private fun expressionTypeForIntArray(arrayExpressionType: ExpressionType, operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.MULTIPLICATION -> ExpressionType.ARRAY(arrayExpressionType)
                else -> null
            }
        }

        private fun expressionTypeForIntTuple(tupleExpressionType: ExpressionType, operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.MULTIPLICATION -> ExpressionType.TUPLE(tupleExpressionType)
                else -> null
            }
        }

        private fun expressionTypeForIntString(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.MULTIPLICATION -> ExpressionType.STRING()
                else -> null
            }
        }

        private fun expressionTypeForIntBool(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.PLUS -> ExpressionType.INT()
                OperationType.MINUS -> ExpressionType.INT()
                OperationType.MULTIPLICATION -> ExpressionType.INT()
                OperationType.DIVISION -> ExpressionType.DOUBLE()
                OperationType.MOD -> ExpressionType.INT()
                OperationType.GREATER -> ExpressionType.BOOL()
                OperationType.LOWER -> ExpressionType.BOOL()
                OperationType.GREATER_EQUAL -> ExpressionType.BOOL()
                OperationType.LOWER_EQUAL -> ExpressionType.BOOL()
                OperationType.POW -> ExpressionType.INT()
                else -> null
            }
        }

        private fun expressionTypeForDoubleBool(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.PLUS -> ExpressionType.DOUBLE()
                OperationType.MINUS -> ExpressionType.DOUBLE()
                OperationType.MULTIPLICATION -> ExpressionType.DOUBLE()
                OperationType.DIVISION -> ExpressionType.DOUBLE()
                OperationType.MOD -> ExpressionType.DOUBLE()
                OperationType.GREATER -> ExpressionType.BOOL()
                OperationType.LOWER -> ExpressionType.BOOL()
                OperationType.GREATER_EQUAL -> ExpressionType.BOOL()
                OperationType.LOWER_EQUAL -> ExpressionType.BOOL()
                OperationType.POW -> ExpressionType.DOUBLE()
                else -> null
            }
        }

        private fun expressionTypeForArrayBool(arrayExpressionType: ExpressionType, operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.MULTIPLICATION -> ExpressionType.ARRAY(arrayExpressionType)
                else -> null
            }
        }

        private fun expressionTypeForTupleBool(tupleExpressionType: ExpressionType, operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.MULTIPLICATION -> ExpressionType.TUPLE(tupleExpressionType)
                else -> null
            }
        }

        private fun expressionTypeForStringArray(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.MOD -> ExpressionType.STRING()
                else -> null
            }
        }

        private fun expressionTypeForStringDictionary(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.MOD -> ExpressionType.STRING()
                else -> null
            }
        }

        private fun expressionTypeForStringBool(operation: OperationType): ExpressionType? {
            return when (operation) {
                OperationType.MULTIPLICATION -> ExpressionType.STRING()
                else -> null
            }
        }
    }
}