package semanticAnalyzer

import model.Method
import model.Symbol
import model.Variable
import java.util.Stack

interface SymbolTable {

    // Adiciona um símbolo com um dado valor na tabela.
    fun add(symbol: Symbol): Symbol

    // Remove o tipo no topo da pilha do símbolo passado por parâmetro. Será utilizado quando o analisador
    // sintático for limpar o escopo atual, por exemplo
    fun remove(symbol: Symbol)

    // Procura por uma variavel na tabela. Se ela não existir retorna nulo
    fun lookUpVariable(name: String): Variable?

    // Procura por um metodo na tabela. Se ele não existir retorna nulo
    fun lookUpMethod(name: String): Method?

    // Utilizado para descobrir o tipo de um símbolo e se ele existe.
    // Pega sempre o ultimo tipo definido no escopo para aquele símbolo
    fun lookUpSymbol(ofName: String): Symbol?

    // Decremente o escopo, restaurando escopo anterior
    fun decrementScope()

    // Incrementa o escopo, indicando que ele foi alterado
    fun incrementScope()
}

class ConcreteSymbolTable: SymbolTable {

    // Adicionar um symbol O(1), remover um symbol O(1)
    // (Isso inclui adicionar esse symbol na stack do hash referente ao seu nome e também na list do topo da pilha scope)
    // Além disso, incrementar o escopo O(1) e decrementar o escopo sera O(n), sendo n o numero de symbols adicionados nesse escopo

    // Mapeia um nome de símbolo para sua pilha de tipos. O valor no topo da pilha de tipos indica a
    // a ultima definição.
    private var symbolsHash: HashMap<String, Stack<Symbol>> = HashMap()
    // Pilha de escopos, onde cada posição contem os names de todos os symbols adicionados nesse escopo
    private var scopeStack: Stack<ArrayList<String>> = Stack<ArrayList<String>>()

    init {
        // Criando o scopo base da tabela
        this.incrementScope()
    }

    override fun add(symbol: Symbol): Symbol {
        if(symbol is Variable) {
            // Checa se a variável está presente na tabela de símbolos
            val symbolLastDefinition = this.lookUpVariable(symbol.name)

            when  {
                // Se a variável não estiver na tabela então adicione-a
                symbolLastDefinition == null -> return addVariableNewEntry(variable = symbol)

                symbolLastDefinition.expressionType == symbol.expressionType -> return updateVariable(newDefinition = symbol, lastDefinition = symbolLastDefinition)

                // Se a variável existir na tabela de símbolos, mas com outro tipo então cheque se ela foi definida no escopo atual
                scopeStack.peek().contains(symbol.name) -> return addVariableAlreadyPresentInCurrentScope(newVariable = symbol, lastDefinition = symbolLastDefinition)

                // Se a variável não existir no escopo atual então adicione-a como uma nova variável
                else -> return addVariableToCurrentScope(variable = symbol)
            }
        } else if (symbol is Method) {
            return symbol
        }
        return symbol // Casos onde não eh metodo nem variavel, não são necessários adicionar na tabela
    }
    override fun remove(symbol: Symbol) {
        if(symbol is Variable) {
            val symbolDefinitions = this.symbolsHash[symbol.name]
            if(!symbolDefinitions.isNullOrEmpty()) {
                symbolDefinitions.pop()
            }
        }

    }

    override fun lookUpVariable(name: String): Variable? {
        // Checa se a pilha de definições de um símbolo com esse nome existe e não está vazia, pois se for um desses
        // casos o símbolo não existe em nenhum escopo.
        if(this.symbolsHash[name]?.isEmpty() == false) {
            // Caso o símbolo exista, pega sua última definição
            val symbolLastDefinition = this.symbolsHash[name]!!.peek()
            // Checa se a definição existente para o símbolo é referente a uma variável
            if (symbolLastDefinition is Variable) {
                symbolLastDefinition.isNewDeclaration = false
                return symbolLastDefinition.copy()
            }
        }
        return null
    }
    override fun lookUpMethod(name: String): Method? {
        return null
    }

    override fun lookUpSymbol(ofName: String): Symbol? {
        // Checa se a pilha de definições de um símbolo com esse nome existe e não está vazia, pois se for um desses
        // casos o símbolo não existe em nenhum escopo.
        if(this.symbolsHash[ofName]?.isEmpty() == false) {
            // Caso o símbolo exista, pega sua última definição
            val symbolLastDefinition = this.symbolsHash[ofName]!!.peek()
            return symbolLastDefinition.copy() as? Symbol
        }
        return null
    }

    override fun incrementScope() {
        val newList = ArrayList<String>()
        scopeStack.push(newList)
    }
    override fun decrementScope() {
        val oldScope = scopeStack.pop()
        for (symbolName in oldScope) {
            val stackSymbol = symbolsHash[symbolName]
            stackSymbol?.pop()
            if (stackSymbol.isNullOrEmpty()) {
                symbolsHash.remove(symbolName)
            }
        }
    }
    /* Adiciona uma variável cujo nome não existe em nenhum escopo, na tabela de símbolos */
    private fun addVariableNewEntry(variable: Variable): Variable {
        val symbolStack = Stack<Symbol>()
        variable.isNewDeclaration = true
        variable.declarationsInCurrentScope = 1
        symbolStack.add(variable)
        this.symbolsHash[variable.name] = symbolStack
        this.scopeStack.peek().add(variable.name)
        return variable.copy()
    }

    /* Atualiza o valor de uma variável que aparece no escopo atual ou em outro anterior */
    private fun updateVariable(newDefinition: Variable, lastDefinition: Variable): Variable {
        newDefinition.declarationsInCurrentScope = lastDefinition.declarationsInCurrentScope
        newDefinition.isNewDeclaration = false
        // O símbolo precisa ser readicionado pois o seu valor pode ter mudado
        this.symbolsHash[newDefinition.name]!!.add(newDefinition)
        return newDefinition.copy()
    }

    /* Adiciona uma variável cujo nome aparece somente em outros escopos, no escopo atual */
    private fun addVariableToCurrentScope(variable: Variable): Variable {
        val variableDefinitions = this.symbolsHash[variable.name]
        variable.isNewDeclaration = true
        variable.declarationsInCurrentScope = 1
        variableDefinitions!!.add(variable)
        this.scopeStack.peek().add(variable.name)
        return variable.copy()
    }
    /* Adiciona uma variável cujo nome já apareceu no escopo atual mas com tipo diferente. Portanto, registra a nova
       declaração */
    private fun addVariableAlreadyPresentInCurrentScope(newVariable: Variable, lastDefinition: Variable): Variable {

        // Se a última definição para o símbolo tem um tipo diferente, então a variável está trocando de tipo
        // e é necessário registrar a nova declaração
        newVariable.declarationsInCurrentScope = lastDefinition.declarationsInCurrentScope + 1
        newVariable.isNewDeclaration = true
        this.symbolsHash[newVariable.name]!!.add(newVariable)
        this.scopeStack.peek().add(newVariable.name)
        return newVariable.copy()
    }
}