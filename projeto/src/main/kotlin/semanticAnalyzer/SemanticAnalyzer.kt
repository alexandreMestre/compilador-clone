package semanticAnalyzer

import model.*

interface SemanticAnalyzer {

    // Utilizado para descobrir o tipo de um símbolo e se ele existe.
    // Pega sempre o ultimo tipo definido no escopo para aquele símbolo
    @Throws(Exception::class)
    fun lookUpSymbol(ofName: String): Symbol?

    // Cria uma variável com um dado tipo.
    fun createVariable(ofName: String, expressionType: ExpressionType, value: String?): Symbol

    // Cria uma variável com um dado tipo.
    fun createMethod(ofName: String, expressionType: ExpressionType, value: String?): Symbol

    // Cria uma constante com um dado tipo.
    fun createConstant(ofValue: String?, expressionType: ExpressionType): Symbol

    // Cria uma lista com um dado tipo.
    fun createList(ofExpressions: ArrayList<Expression>, value: String?): Symbol

    // Cria uma Tupla com um dado tipo.
    fun createTuple(ofExpressions: ArrayList<Expression>, value: String?): Symbol

    // Cria uma operacao entre duas expressoes
    @Throws(Exception::class)
    fun createOperation(ofType: OperationType, firstExpression: Expression, secondExpression: Expression): Operation

    // Cria uma operacao unaria
    @Throws(Exception::class)
    fun createOperation(ofType: OperationType, expression: Expression): Operation

    // Verifica se uma expression eh iteravel e retorna um erro caso nao seja
    @Throws(Exception::class)
    fun getIterableExpressionSubtype(expression: Expression): ExpressionType

    // Decremente o escopo da tabela de simbolos, restaurando escopo anterior
    fun decrementScope()

    // Incrementa o escopo da tabela de simbolos, indicando que ele foi alterado
    fun incrementScope()
}

class ConcreteSemanticAnalyzer: SemanticAnalyzer {
    // Tabela de símbolos que vai controlar os tipos dos símbolos definidos
    var symbolTable: SymbolTable

    constructor(symbolTable: SymbolTable) {
        this.symbolTable = symbolTable
    }

    @Throws(Exception::class)
    override fun lookUpSymbol(ofName: String): Symbol? {
        return symbolTable.lookUpSymbol(ofName = ofName)
    }

    override fun createVariable(ofName: String, expressionType: ExpressionType, value: String?): Symbol {
        val variable = ConcreteVariable(expressionType = expressionType, value = value, name = ofName)
        return symbolTable.add(variable)
    }
    override fun createMethod(ofName: String, expressionType: ExpressionType, value: String?): Symbol {
        val method = ConcreteMethod(expressionType = expressionType, value = value, name = ofName)
        return symbolTable.add(method)
    }
    override fun createConstant(ofValue: String?, expressionType: ExpressionType): Symbol {
        val constant = ConcreteConstant(expressionType = expressionType, value = ofValue)
        return symbolTable.add(constant)
    }

    @Throws(Exception::class)
    override fun createList(ofExpressions: ArrayList<Expression>, value: String?): Symbol {
        val arrayExpressionType = getUniqueArrayExpressionType(ofExpressions)
        val list = ConcreteList(ofExpressions, ExpressionType.ARRAY(arrayExpressionType), value)
        return symbolTable.add(list)
    }

    @Throws(Exception::class)
    private fun getUniqueArrayExpressionType(ofExpressions: ArrayList<Expression>): ExpressionType {
        var firstExpression: Expression = if (ofExpressions.isNotEmpty()) ofExpressions.first() else return ExpressionType.EMPTY()
        for (expression in ofExpressions) {
            if (expression.expressionType != firstExpression.expressionType) {
                throw SemanticAnalyzerException.List(previousExpression = firstExpression, currentExpression = expression)
            }
        }
        return firstExpression.expressionType
    }

    @Throws(Exception::class)
    override fun createTuple(ofExpressions: ArrayList<Expression>, value: String?): Symbol {
        val tupleExpressionType = getUniqueTupleExpressionType(ofExpressions)
        val tuple = ConcreteTuple(ofExpressions, ExpressionType.TUPLE(tupleExpressionType), value)
        return symbolTable.add(tuple)
    }

    @Throws(Exception::class)
    private fun getUniqueTupleExpressionType(ofExpressions: ArrayList<Expression>): ExpressionType {
        var firstExpression: Expression = if (ofExpressions.isNotEmpty()) ofExpressions.first() else return ExpressionType.EMPTY()
        for (expression in ofExpressions) {
            if (expression.expressionType != firstExpression.expressionType) {
                throw SemanticAnalyzerException.Tuple(previousExpression = firstExpression, currentExpression = expression)
            }
        }
        return firstExpression.expressionType
    }

    @Throws(Exception::class)
    override fun createOperation(ofType: OperationType, firstExpression: Expression, secondExpression: Expression): Operation {
        val expressionType = OperationsTable.expressionType(
            firstExpressionType = firstExpression.expressionType,
            secondExpressionType = secondExpression.expressionType,
            operationType = ofType
        )
        val operation = ConcreteOperation(
            operationType = ofType,
            firstOperator = firstExpression,
            secondOperator = secondExpression,
            expressionType = expressionType
        )
        return operation
    }

    @Throws(Exception::class)
    override fun createOperation(ofType: OperationType, expression: Expression): Operation {
        val expressionType = OperationsTable.expressionType(
            firstExpressionType = expression.expressionType,
            operationType = ofType
        )
        val operation = ConcreteOperation(
            operationType = ofType,
            firstOperator = expression,
            secondOperator = null,
            expressionType = expressionType
        )
        return operation
    }

    @Throws(Exception::class)
    override fun getIterableExpressionSubtype(expression: Expression): ExpressionType {
        val type = expression.expressionType
        when (type) {
            is ExpressionType.ARRAY -> return type.expressionType
            // TODO: Return keyExpressionType
            is ExpressionType.DICTIONARY -> return ExpressionType.STRING()
            is ExpressionType.STRING -> return ExpressionType.STRING()
            else -> throw SemanticAnalyzerException.Iterable(expression)
        }
    }

    override fun decrementScope() {
        symbolTable.decrementScope()
    }

    override fun incrementScope() {
        symbolTable.incrementScope()
    }
}