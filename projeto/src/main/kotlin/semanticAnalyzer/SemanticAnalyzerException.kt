package semanticAnalyzer

import model.OperationType
import model.ExpressionType
import model.Expression

class SemanticAnalyzerException {
    class Operation: Exception {

        constructor(operationType: OperationType, firstExpressionType: ExpressionType, secondExpressionType: ExpressionType) : super(
            "Não é possível realizar a operação "
            + operationType.name
            + " entre operadores dos tipos "
            + firstExpressionType.name
            + " e "
            + secondExpressionType.name
        )

        constructor(operationType: OperationType, expressionType: ExpressionType) : super(
            "Não é possível realizar a operação "
            + operationType.name
            + " no operador do tipo "
            + expressionType.name
        )
    }

    class List: Exception {
        constructor(previousExpression: Expression, currentExpression: Expression): super(
            "Lista com tipos diferentes: "
            + previousExpression.expressionType.name
            + " e "
            + currentExpression.expressionType.name
        )
    }

    class Tuple: Exception {
        constructor(previousExpression: Expression, currentExpression: Expression): super(
            "Tupla com tipos diferentes: "
            + previousExpression.expressionType.name
            + " e "
            + currentExpression.expressionType.name
        )
    }

    class Iterable: Exception {
        constructor(expression: Expression): super(
            "A expressão do tipo "
            + expression.expressionType
            + " não é iterável."
        )
    }
}