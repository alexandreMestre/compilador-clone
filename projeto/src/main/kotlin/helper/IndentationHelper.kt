package helper

import java.lang.Exception

interface IndentationHelper {
    fun getScopeVariation(identation: String): Int
    fun saveScopeVariation(identation: String)
}

class ConcreteIndentationHelper: IndentationHelper {
    private var lastIndentations = mutableListOf(Pair(0, 0))
    private var currentTabIndentation = 0
    private var currentSpaceIndentation = 0

    // calcula a variação de escopos para a identação informada
    // 1  - Indica que iniciou 1 novo escopo
    // 0  - Indica que continua no mesmo escopo
    // -n - Indica que encerrou os n escopos anteriores
    override fun getScopeVariation(identation: String): Int {
        this.startNewIndentation()
        for (char in identation) {
            if (char == ' ') {
                this.addSpaceIndentation()
            } else {
                this.addTabIndentation()
            }
        }
        return getScopeVariation(false)
    }

    // salva a variação de escopos para a identação informada
    override fun saveScopeVariation(identation: String) {
        getScopeVariation(true)
    }

    // Começa a computar a indentação de uma nova linha
    private fun startNewIndentation() {
        currentSpaceIndentation = 0
        currentTabIndentation = 0
    }

    // Adiciona mais um espaço na indentação
    private fun addSpaceIndentation() {
        currentSpaceIndentation += 1
    }

    // Adiciona mais um tab na indentação
    private fun addTabIndentation() {
        currentTabIndentation += 1
    }

    // calcula a variação de escopos.
    // 1  - Indica que iniciou 1 novo escopo
    // 0  - Indica que continua no mesmo escopo
    // -n - Indica que encerrou os n escopos anteriores
    private fun getScopeVariation(shouldSave: Boolean): Int {
        if (Pair(currentTabIndentation, currentSpaceIndentation) == lastIndentations.first()) {
            return 0
        } else if (currentTabIndentation >= lastIndentations.first().first &&
            currentSpaceIndentation >= lastIndentations.first().second) {

            if (shouldSave) {
                // salva a indentação atual e começa um novo escopo
                lastIndentations.add(0, Pair(currentTabIndentation, currentSpaceIndentation))
            }
            // retorna que um novo escopo foi criado
            return  1
        } else {
            // procura do escopo mais recente ao escopo mais antigo, um que tenha a mesma identação que
            // a linha atual. O índice deste escopo na lista será igual ao número de escopos encerrados
            val endedScopes = lastIndentations.indexOfFirst { indentation ->
                indentation == Pair(currentTabIndentation, currentSpaceIndentation)
            }

            // se não foi encontrado nenhum escopo com a mesma indentação, lança uma exceção
            if (endedScopes == -1) {
                throw Exception("Unexpected indentation")
            }

            if (shouldSave) {
                // remove da lista os escopos que foram encerrados
                for (i in 0 until endedScopes) {
                    lastIndentations.removeAt(0)
                }
            }

            // retorna o número de escopos encerrados
            return  -endedScopes
        }
    }
}