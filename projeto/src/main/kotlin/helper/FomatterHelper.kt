package helper

class FormatterHelper {
    companion object {

        /**
         * Used to convert raw characters to their escaped version
         * when these raw version cannot be used as part of an ASCII
         * string literal.
         */
        @JvmStatic
        fun add_escapes(str: String): String {
            return str.fold("") { result, char ->
                result + when (char) {
                    '\b' -> "\\b"
                    '\t' -> "\\t"
                    '\n' -> "\\n"
                    '\r' -> "\\r"
                    '\"' -> "\\\""
                    '\'' -> "\\\'"
                    else -> "$char"
                }
            }
        }
    }
}