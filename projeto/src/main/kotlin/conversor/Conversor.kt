package conversor

import model.*
import java.io.File

// Symbol: estrutura que também é uma Expression e que é utilizada pra representar variaveis, metodos e constantes. É relacionada a tabela de símbolos do analisador semântico
// Operation: estrutura que também é uma expression e é composto por duas Expressions, ou seja, é composto por dois símbolos, ou duas operations, ou um simbolo e uma operation.
// Se analisando uma Expression vocês descobrem que ela é do tipo Symbol, e em seguida descobrem que é uma Variable, usar campos 'declarationsInCurrentScope' e 'isNewDeclaration' para saber o numero de vezes que ela foi declarada naquele escopo mudando de tipo (p.e. a = "abc"; a = 2; a = 2.5; declarationsInCurrentScope será 0, 1 e 2, respectivamente, ou seja, vocês terão que criar um nome diferente pra essa variável em C pros casos em que ela é 1 e 2).
// o campo isNewDclaration indica no caso se a variavel está sendo declarada nesse statement ou não, só está sendo utilizada.

interface Conversor {
    fun addVarAssignment(variable: Variable, expression: Expression)

    fun verifyTupleType(expression: Expression):String
    fun getSizeTuple(tuple: Tuple): String

    fun verifyListType(expression: Expression):String
    fun getSizeList(list: model.List): String

    fun startIfDeclaration(condition: Expression)
    fun startElifDeclaration(condition: Expression)
    fun startElseDeclaration()
    fun endIfDeclaration()

    fun startForDeclaration(variable: Variable, iterableElement: Expression)
    fun endForDeclaration()

    fun startWhileDeclaration(condition: Expression)
    fun endWhileDeclaration()

    fun generateFile()
}

class ConcreteConversor: Conversor {
    private var includeStatements = listOf("#include<stdio.h>", "#include<math.h>", "#include \"data_structs\".h")
    private var definesStatements = listOf(String())
    private var variablesDeclarationsStatements = listOf(String())
    private var methodsHeadersStatements = listOf(String())
    private var mainStatements = listOf(String())
    private var methodsImplementationStatements = listOf(String())

    private val conversorHelper: ConversorHelper
    private val outputPah: String?

    private var identationSpaces = 0

    constructor(conversorHelper: ConversorHelper) {
        this.conversorHelper = conversorHelper
        this.outputPah = null
    }

    constructor(conversorHelper: ConversorHelper, outputPah: String?) {
        this.conversorHelper = conversorHelper
        this.outputPah = outputPah
    }

    override fun generateFile() {
        print(includeStatements)
        print(definesStatements)
        print(variablesDeclarationsStatements)
        print(methodsHeadersStatements)
        print(listOf("int main() {"))
        print(mainStatements, "\t")
        print(listOf("return 0;\n}"), "\t")
        print(methodsImplementationStatements)
    }

    private fun print(statements: List<String>, indentation: String = "") {
        // send output to file if path is specified
        if (outputPah != null) {
            statements.forEach { line ->
                File(outputPah).appendText("$indentation$line\n")
            }

            if (!statements.isEmpty()) {
                File(outputPah).appendText("\n")
            }
        } else { // print to std out
            statements.forEach { line ->
                println("$indentation$line")
            }

            if (!statements.isEmpty()) {
                println()
            }
        }
    }

    private fun currentIndentation(): String {
        return "\t".repeat(identationSpaces)
    }

    override fun addVarAssignment(variable: Variable, expression: Expression) {
        var declarationStatement = ""
        var expressionStatement = ""

        if (variable.isNewDeclaration) {
            declarationStatement += when(variable.expressionType) {
                is ExpressionType.INT -> "int "
                is ExpressionType.DOUBLE -> "double "
                is ExpressionType.ARRAY -> verifyListType(expression)
                is ExpressionType.TUPLE -> verifyTupleType(expression)
                is ExpressionType.STRING -> "char *"
                is ExpressionType.DICTIONARY -> ""
                is ExpressionType.BOOL -> "int "
                is ExpressionType.EMPTY -> ""
                is ExpressionType.ERROR -> ""
            }

            if (variable.expressionType is ExpressionType.TUPLE){
                declarationStatement += "*${variable.name}_${variable.declarationsInCurrentScope};"
            }
            else if(variable.expressionType is ExpressionType.ARRAY) {
                declarationStatement += " *${variable.name}_${variable.declarationsInCurrentScope};"
            }

            else {
                declarationStatement += "${variable.name}_${variable.declarationsInCurrentScope};"
            }
        }


        expressionStatement += conversorHelper.resolve(expression)

        if (!expressionStatement.isEmpty()) {
            mainStatements += currentIndentation() +
                    "${variable.name}_${variable.declarationsInCurrentScope} = $expressionStatement;"
        }

        if (!declarationStatement.isEmpty() && declarationStatement !in variablesDeclarationsStatements) {
            variablesDeclarationsStatements += declarationStatement
        }
    }

    override fun verifyListType(expression: Expression): String {

        var listType = ""
        listType = when((expression.expressionType as ExpressionType.ARRAY).expressionType) {
            is ExpressionType.INT -> "ListInt"
            is ExpressionType.DOUBLE -> "ListDouble"
            is ExpressionType.STRING -> "ListString"
            else -> ""
        }

        return listType

    }

    override fun verifyTupleType(expression: Expression):String {
        var tupleType = ""
        tupleType = when((expression.expressionType as ExpressionType.TUPLE).expressionType) {
            is ExpressionType.INT -> "tupleFieldInteger "
            is ExpressionType.DOUBLE -> "tupleFieldReal "
            is ExpressionType.STRING -> "tupleFieldString "
            is ExpressionType.BOOL -> "tupleFieldInteger "
            else -> ""
        }
        return tupleType
    }

    override fun getSizeList(list: model.List): String {

        var listExp = ""
        var size = 0

        for (item in list.expressions)  {
            size += 1
        }

        listExp += ", $size);"

        return listExp
    }

    override fun getSizeTuple(tuple: Tuple): String {
        var tupleExp = ""
        var size = 0

        for (item in tuple.expressions){
            size += 1
        }

        tupleExp += ", $size);"
        return tupleExp
    }

    override fun startIfDeclaration(condition: Expression) {
        var ifDeclaration = ""

        ifDeclaration += "if(${conversorHelper.resolve(condition)}) {"
        mainStatements += currentIndentation()+ifDeclaration
        identationSpaces += 1

    }

    override fun startElifDeclaration(condition: Expression) {
        var elifDeclaration = ""

        identationSpaces -= 1
        elifDeclaration += "} else if(${conversorHelper.resolve(condition)}) {"
        mainStatements += currentIndentation() + elifDeclaration
        identationSpaces += 1
    }

    override fun startElseDeclaration() {
        var elseDeclaration = ""

        identationSpaces -= 1
        elseDeclaration += "} else {"
        mainStatements += currentIndentation() + elseDeclaration
        identationSpaces += 1
    }

    override fun endIfDeclaration() {
        endCodeBlock()
    }

    // LOOPS

    override fun startForDeclaration(variable: Variable, iterableElement: Expression) {
        var initialization = ""
        var pointerName = "forLoopListHeader_$identationSpaces"
        var pointerType = "List"
        var listVariableName = "auxiliarListLoop_$identationSpaces"
        var elementsType = ""

        // resolving array type into list pointer structure
        val iterableType = iterableElement.expressionType
        if (iterableType is ExpressionType.ARRAY) {
            elementsType = resolveArrayType(iterableType)
            pointerType += "${elementsType.capitalize()}*"
        }

        if (iterableElement is Variable) {
            listVariableName = conversorHelper.resolveVariable(iterableElement)
        } else {
            // if handling a list, create a variable to hold it (e.g for in in [1, 2,3] in python)
            initialization = """$pointerType $listVariableName = ${conversorHelper.resolve(iterableElement)}"""
        }

        val forInitStatement = "$pointerType $pointerName = $listVariableName"
        val forConditionStatement = "$pointerName != NULL"
        val forIncrementStatement = "$pointerName = $pointerName->next"
        val forStatement = "for($forInitStatement; $forConditionStatement; $forIncrementStatement) {"
        val loopVariableAttribution = "$elementsType ${conversorHelper.resolveVariable(variable)} = $pointerName->value;"

        mainStatements += currentIndentation() + initialization
        mainStatements += currentIndentation() + forStatement
        identationSpaces += 1
        mainStatements += currentIndentation() + loopVariableAttribution
    }

    private fun resolveArrayType(expressionType: ExpressionType.ARRAY): String {
        return when(expressionType.expressionType) {
            is ExpressionType.INT -> "int"
            is ExpressionType.DOUBLE -> "double"
            is ExpressionType.STRING -> "string"
            is ExpressionType.BOOL -> "bool"
            //TODO: decide how to handle array of array, tuple, dictionary and empty arrays
            is ExpressionType.ARRAY -> resolveArrayType(expressionType.expressionType)
            is ExpressionType.TUPLE -> ""
            is ExpressionType.DICTIONARY -> ""
            is ExpressionType.EMPTY -> ""
            is ExpressionType.ERROR -> ""
        }
    }

    override fun endForDeclaration() {
        endCodeBlock()
    }

    override fun startWhileDeclaration(condition: Expression) {
        val whileDeclaration = "while(${conversorHelper.resolve(condition)}) {"
        mainStatements += currentIndentation() + whileDeclaration
        identationSpaces += 1
    }

    override fun endWhileDeclaration() {
        endCodeBlock()
    }

    private fun endCodeBlock() {
        identationSpaces -= 1
        mainStatements += currentIndentation() + "}"
    }
}

// Metodos que irao converter as coisas para formato de saída do arquivo em C
interface ConversorHelper {
    fun resolve(expression: Expression): String
    fun resolveVariable(variable: Variable): String
    fun resolveConstant(constant: Constant): String
    fun resolveMethod(method: Method): String
    fun resolveOperation(operation: Operation): String
    fun resolveTuple(tuple: Tuple): String
    fun resolveList(list: model.List): String
}

class ConcreteConversorHelper: ConversorHelper {
    override fun resolve(expression: Expression): String {
        return when(expression) {
            is Variable -> resolveVariable(expression)
            is Constant -> resolveConstant(expression)
            is Method -> resolveMethod(expression)
            is Operation -> resolveOperation(expression)
            is Tuple -> resolveTuple(expression)
            is model.List -> resolveList(expression)
            else -> ""
        }
    }

    override  fun resolveList(list: model.List): String {
        var listExp = ""
        var size = 0

        for (item in list.expressions){
            size += 1
        }

        if((list.expressionType as ExpressionType.ARRAY).expressionType is ExpressionType.INT) {
            listExp += "CreateListInt( (int[]) { "
        } else if ((list.expressionType as ExpressionType.ARRAY).expressionType is ExpressionType.DOUBLE){
            listExp += "CreateListDouble( (double[]) { "
        } else if ((list.expressionType as ExpressionType.ARRAY).expressionType is ExpressionType.STRING){
            listExp += "CreateListString( (char*) { "
        }

        for (item in list.expressions){
            listExp += this.resolve(item) + ", "
        }
        listExp = listExp.substring(0, listExp.length - 2)
        listExp += " }, ${ list.expressions.size })"

        return listExp
    }
    override fun resolveTuple(tuple: Tuple): String {
        var tupleExp = ""
        var size = 0

        for (item in tuple.expressions){
            size += 1
        }

        if((tuple.expressionType as ExpressionType.TUPLE).expressionType is ExpressionType.INT) {
            tupleExp += "CreateTupleInteger( (int[]) {"
        }
        else if ((tuple.expressionType as ExpressionType.TUPLE).expressionType is ExpressionType.DOUBLE){
            tupleExp += "CreateTupleReal( (double[]) {"
        }
        else if ((tuple.expressionType as ExpressionType.TUPLE).expressionType is ExpressionType.STRING){
            tupleExp += "CreateTupleString( (char*) {"
        }
        else if ((tuple.expressionType as ExpressionType.TUPLE).expressionType is ExpressionType.BOOL){
            tupleExp += "CreateTupleInteger( (int[]) {"
        }

        for (item in tuple.expressions){
            tupleExp += this.resolve(item) + ", "
        }
        tupleExp = tupleExp.substring(0, tupleExp.length - 2)
        tupleExp += " }, ${ tuple.expressions.size })"

        return tupleExp
    }

    override fun resolveVariable(variable: Variable): String {
        return "${variable.name}_${variable.declarationsInCurrentScope}"
    }

    override fun resolveConstant(constant: Constant): String {
        return constant.value ?: ""
    }

    override fun resolveMethod(method: Method): String {
        return ""
    }

    override fun resolveOperation(operation: Operation): String {
        val firstExpDescription = resolve(operation.firstOperator)
        var secondExpDescription = ""
        if (operation.secondOperator != null) {
            secondExpDescription = resolve(operation.secondOperator!!)
        }

        if (operation.operationType == OperationType.POW) {
            return "pow($firstExpDescription,$secondExpDescription)"
        }

        if(operation.firstOperator.expressionType is ExpressionType.STRING) {
            val operatorDescription: String = when (operation.operationType) {
                OperationType.PLUS -> "ConcatenateStrings($firstExpDescription, $secondExpDescription)"
                OperationType.MINUS -> ""
                OperationType.MULTIPLICATION -> ""
                OperationType.DIVISION -> ""
                OperationType.MOD -> ""
                OperationType.EQUAL -> "EqualStrings($firstExpDescription, $secondExpDescription)"
                OperationType.GREATER -> "GreaterEqualStrings($firstExpDescription, $secondExpDescription)"
                OperationType.GREATER_EQUAL -> "GreaterEqualStrings($firstExpDescription, $secondExpDescription)"
                OperationType.LOWER -> "LowerEqualStrings($firstExpDescription, $secondExpDescription)"
                OperationType.LOWER_EQUAL -> "LowerEqualStrings($firstExpDescription, $secondExpDescription)"
                OperationType.NOT_EQUAL -> ""
                OperationType.AND -> ""
                OperationType.OR -> ""
                OperationType.NOT -> ""
                OperationType.POW -> ""
            }
            return "$operatorDescription"

        }
        else if (operation.firstOperator.expressionType is ExpressionType.ARRAY) {

            var declarationType = ""
            if((operation.firstOperator.expressionType as ExpressionType.ARRAY).expressionType is ExpressionType.INT) {
                declarationType = "ListInt"

            }
            else if ((operation.firstOperator.expressionType as ExpressionType.ARRAY).expressionType is ExpressionType.DOUBLE){
                declarationType = "ListDouble"

            }

            else if ((operation.firstOperator.expressionType as ExpressionType.ARRAY).expressionType is ExpressionType.STRING){
                declarationType = "ListString"

            }

            val operatorDescription: String = when (operation.operationType) {
                OperationType.PLUS -> "Concatenate$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.MINUS -> ""
                OperationType.MULTIPLICATION -> ""
                OperationType.DIVISION -> ""
                OperationType.MOD -> ""
                OperationType.EQUAL -> "Equal$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.GREATER -> "GreaterEqual$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.GREATER_EQUAL -> "GreaterEqual$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.LOWER -> "LowerEqual$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.LOWER_EQUAL -> "LowerEqual$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.NOT_EQUAL -> ""
                OperationType.AND -> ""
                OperationType.OR -> ""
                OperationType.NOT -> ""
                OperationType.POW -> ""
            }

            return "$operatorDescription"
        }

        else if (operation.firstOperator.expressionType is ExpressionType.TUPLE) {

            var declarationType = ""
            if((operation.firstOperator.expressionType as ExpressionType.TUPLE).expressionType is ExpressionType.INT) {
                declarationType = "TupleInteger"

            }
            else if ((operation.firstOperator.expressionType as ExpressionType.TUPLE).expressionType is ExpressionType.DOUBLE){
                declarationType = "TupleReal"

            }

            else if ((operation.firstOperator.expressionType as ExpressionType.TUPLE).expressionType is ExpressionType.STRING){
                declarationType = "TupleString"

            }

            val operatorDescription: String = when (operation.operationType) {
                OperationType.PLUS -> "Concatenate$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.MINUS -> ""
                OperationType.MULTIPLICATION -> ""
                OperationType.DIVISION -> ""
                OperationType.MOD -> ""
                OperationType.EQUAL -> "Equal$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.GREATER -> "GreaterEqual$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.GREATER_EQUAL -> "GreaterEqual$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.LOWER -> "LowerEqual$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.LOWER_EQUAL -> "LowerEqual$declarationType($firstExpDescription, $secondExpDescription)"
                OperationType.NOT_EQUAL -> ""
                OperationType.AND -> ""
                OperationType.OR -> ""
                OperationType.NOT -> ""
                OperationType.POW -> ""
            }

            return "$operatorDescription"

        }

        else {
            val operatorDescription: String = when (operation.operationType) {
                OperationType.PLUS -> "+"
                OperationType.MINUS -> "-"
                OperationType.MULTIPLICATION -> "*"
                OperationType.DIVISION -> "/"
                OperationType.MOD -> "%"
                OperationType.EQUAL -> "=="
                OperationType.GREATER -> ">"
                OperationType.GREATER_EQUAL -> ">="
                OperationType.LOWER -> "<"
                OperationType.LOWER_EQUAL -> "<="
                OperationType.NOT_EQUAL -> "!="
                OperationType.AND -> "&&"
                OperationType.OR -> "||"
                OperationType.NOT -> "!"
                OperationType.POW -> ""
            }
            return "($firstExpDescription) $operatorDescription ($secondExpDescription)"
        }

        return ""
    }
}
