package model

interface Symbol: Expression {
    var value: String? // Propriedade somente é adicionada quando ocorre uma atribuição a variavel
}

interface Variable: Symbol {
    var name: String
    var declarationsInCurrentScope: Int
    var isNewDeclaration: Boolean

    override fun copy(): Variable
}

interface Method: Symbol {
    var name: String
}

interface Constant: Symbol {}

interface List: Symbol {
    var expressions: ArrayList<Expression>
}

interface Tuple: Symbol {
    var expressions: ArrayList<Expression>
}

class ConcreteVariable: Variable {
    override var expressionType: ExpressionType
    override var value: String?
    override var name: String
    override var declarationsInCurrentScope: Int
    override var isNewDeclaration: Boolean

    constructor(expressionType: ExpressionType, value: String?, name: String) {
        this.expressionType = expressionType
        this.value = value
        this.name = name
        this.declarationsInCurrentScope = 0
        this.isNewDeclaration = false
    }

    constructor(expressionType: ExpressionType, value: String?, name: String, declarationsInCurrentScope: Int, isNewDeclaration: Boolean) {
        this.expressionType = expressionType
        this.value = value
        this.name = name
        this.declarationsInCurrentScope = declarationsInCurrentScope
        this.isNewDeclaration = isNewDeclaration
    }

    override fun copy(): Variable {
        return ConcreteVariable(expressionType, value, name, declarationsInCurrentScope, isNewDeclaration)
    }
}

class ConcreteMethod: Method {
    override var expressionType: ExpressionType
    override var value: String?
    override var name: String

    // TODO: Talvez tenhamos que adicionar uma estrutura para controlar as variaveis do escopo do metodo

    constructor(expressionType: ExpressionType, value: String?, name: String) {
        this.expressionType = expressionType
        this.value = value
        this.name = name
    }

    override fun copy(): Symbol {
        TODO("Métodos ainda não são abordados no projeto")
    }
}

class ConcreteConstant(override var expressionType: ExpressionType, override var value: String?): Constant {
    override fun copy(): Symbol {
        return ConcreteConstant(this.expressionType, this.value)
    }
}

class ConcreteList: List {
    override var expressions: ArrayList<Expression>
    override var expressionType: ExpressionType
    override var value: String?

    constructor(expressions: ArrayList<Expression>, expressionType: ExpressionType, value: String?) {
        this.expressions = expressions
        this.expressionType = expressionType
        this.value = value
    }

    override fun copy(): Symbol {
        val copiedExpressions = ArrayList(expressions.map { it.copy() })
        return ConcreteList(copiedExpressions, expressionType, value)
    }
}

class ConcreteTuple: Tuple {
    override var expressions: ArrayList<Expression>
    override var expressionType: ExpressionType
    override var value: String?

    constructor(expressions: ArrayList<Expression>, expressionType: ExpressionType, value: String?) {
        this.expressions = expressions
        this.expressionType = expressionType
        this.value = value
    }

    override fun copy(): Symbol {
        val copiedExpressions = ArrayList(expressions.map { it.copy() })
        return ConcreteTuple(copiedExpressions, expressionType, value)
    }
}