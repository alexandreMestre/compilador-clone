package model

import kotlin.collections.List

interface Expression {
    fun copy(): Expression
    var expressionType: ExpressionType
}

sealed class ExpressionType {
    class INT: ExpressionType()
    class DOUBLE: ExpressionType()
    class STRING: ExpressionType()
    class BOOL: ExpressionType()
    class ARRAY(val expressionType: ExpressionType): ExpressionType()
    class TUPLE(val expressionType: ExpressionType): ExpressionType()
    class DICTIONARY: ExpressionType()
    class EMPTY: ExpressionType() // para arrays, tuplas e dicionarios vazios
    class ERROR: ExpressionType()
    
    val name: String
        get() = when (this) {
            is ExpressionType.INT -> "integer"
            is ExpressionType.DOUBLE -> "double"
            is ExpressionType.STRING -> "string"
            is ExpressionType.BOOL -> "boolean"
            is ExpressionType.ARRAY -> "list of " + this.expressionType.name
            is ExpressionType.TUPLE -> "tuple of " + this.expressionType.name
            is ExpressionType.DICTIONARY -> "dictionary"
            is ExpressionType.EMPTY -> "empty"
            is ExpressionType.ERROR -> "error"
        }

    override fun equals(other: Any?): Boolean {
        return this.name == (other as? ExpressionType)?.name
    }

    companion object {
        val values: List<ExpressionType>
            get() = values()

        private fun values(): List<ExpressionType> {
            var list = listOf(
                ExpressionType.INT(),
                ExpressionType.DOUBLE(),
                ExpressionType.STRING(),
                ExpressionType.BOOL(),
                ExpressionType.ARRAY(ExpressionType.EMPTY()),
                ExpressionType.TUPLE(ExpressionType.EMPTY()),
                ExpressionType.DICTIONARY(),
                ExpressionType.EMPTY(),
                ExpressionType.ERROR()
            )
            val allExpressionTypes = list
            for (expressionType in allExpressionTypes) {
                list = list + ExpressionType.ARRAY(expressionType)
                list = list + ExpressionType.TUPLE(expressionType)
            }
            return list
        }
    }
}