package model

interface Token {}

enum class OperationType: Token {
    PLUS, MINUS, MULTIPLICATION, DIVISION, MOD, POW,
    EQUAL, GREATER, GREATER_EQUAL, LOWER, LOWER_EQUAL, NOT_EQUAL,
    AND, OR, NOT;
}

interface Operation: Expression {
    val operationType: OperationType
    val firstOperator: Expression
    val secondOperator: Expression?
}

class ConcreteOperation: Operation {
    override val operationType: OperationType
    override val firstOperator: Expression
    override val secondOperator: Expression?
    override var expressionType: ExpressionType

    @Throws(Exception::class)
    constructor(operationType: OperationType, firstOperator: Expression, secondOperator: Expression?, expressionType: ExpressionType) {
        this.operationType = operationType
        this.firstOperator = firstOperator
        this.secondOperator = secondOperator
        this.expressionType = expressionType
    }

    override fun copy(): Expression {
        return ConcreteOperation(this.operationType, this.firstOperator.copy(), this.secondOperator?.copy(), this.expressionType)
    }
}