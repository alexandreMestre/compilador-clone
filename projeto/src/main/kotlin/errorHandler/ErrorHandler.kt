package errorHandler

import java.lang.Exception

interface ErrorHandler {
    fun handleError(error: Error)
    fun handleException(exception: Exception)
    fun exceptionList(): ArrayList<Exception>
}