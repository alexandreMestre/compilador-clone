package errorHandler

class CompilerException(message: String, val line: Int, val column: Int): Exception("$message. Erro encontrado na linha: $line e coluna: $column")