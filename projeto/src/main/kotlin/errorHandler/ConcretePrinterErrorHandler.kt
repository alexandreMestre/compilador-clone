package errorHandler

import helper.FormatterHelper
import java.lang.Exception

private var exceptionList = ArrayList<Exception>()
private var errorList = ArrayList<Error>()

class ConcretePrinterErrorHandler: ErrorHandler {
    override fun handleError(error: Error) {
        println(FormatterHelper.add_escapes(error.message ?: ""))
        errorList.add(error)
    }

    override fun handleException(exception: Exception) {
        println(FormatterHelper.add_escapes(exception.message ?: ""))
        exceptionList.add(exception)
    }

    override fun exceptionList(): ArrayList<Exception> {
        return exceptionList
    }
}