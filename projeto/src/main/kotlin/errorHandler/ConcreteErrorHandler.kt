package errorHandler

import java.lang.Exception

class ConcreteErrorHandler: ErrorHandler {

    private var exceptionList = ArrayList<Exception>()
    private var errorList = ArrayList<Error>()

    override fun handleError(error: Error) {
        errorList.add(error)
    }

    override fun handleException(exception: Exception) {
        exceptionList.add(exception)
    }

    override fun exceptionList(): ArrayList<Exception> {
        return exceptionList
    }
}