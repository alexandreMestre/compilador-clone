.PHONY: help

.DEFAULT_GOAL := help

REPOSITORY := 'franciscorafael/docker-grupo06'
SHELL := /bin/bash

NAME   := docker-grupo06
ORG    := franciscorafael
REPO   := ${ORG}/${NAME}
TAG    := $(shell git log -1 --pretty=format:"%h")
IMG    := ${REPO}:${TAG}
LATEST := ${REPO}:latest


pull:
	docker pull franciscorafael/docker-grupo06

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

release: 
	bash -c '[[ -z `git status -s` ]]'
	git tag -a -m release $(VERSION)
	git push --tags

build: 
	docker build --tag $(REPOSITORY) --rm=false .

login:
	docker login -e $DOCKER_EMAIL -u $DOCKER_USER -p $DOCKER_PASSWORD

push: 
	docker push $(REPOSITORY)

run: 
	docker run -it --rm --volume=$(HOME):$(HOME) -w=$(HOME) $(REPOSITORY)

ci-push: ci-login  
	docker build -t ${IMG} -t ${LATEST} .
	docker push ${IMG}
	docker push ${LATEST}

ci-push-tag: ci-login
	docker build -t ${REPO}:${TAG} .
	docker push ${REPO}:${TAG}
